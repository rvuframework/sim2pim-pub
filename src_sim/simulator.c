#include "simulator.h"

void* SimulatorPIM(void* args) {//PIM Simulation Thread

    uint64_t* instructionBuffer = malloc(sizeof(uint64_t)*2*Conf.numProc);
    uint64_t* LSBuffer = malloc(sizeof(uint64_t)*Conf.numProc);
    uint64_t* cycleBuffer = malloc(sizeof(uint64_t)*Conf.numProc);

    int t_core = sched_getcpu();

    printf("PIM: Simulator at core %d\n", t_core);

    //Define PIM internal Registers

		while (atomic_flag_test_and_set(PIM_start_flag)) {//Thread Loop
      for (size_t i = 0; i < Conf.numProc; i++) {//Check all buffers
          while(atomic_flag_test_and_set(FIFO_flag+i))
            ;//Loop while FIFO_flag is locked (1), if unlocked acquire and end Loop
          BufferConsume(instructionBuffer, LSBuffer, cycleBuffer, i);//get first position of buffer i
          atomic_flag_clear(FIFO_flag+i);//unlock FIFO_flag
      }

      for (size_t i = 0; i < Conf.numProc; i++) {
        if ((instructionBuffer[i] != 0) ||
            (instructionBuffer[i+Conf.numProc] != 0))
            {
              // printf("PIM: %ld Instruction = %lx %lx\n", i,instructionBuffer[i], instructionBuffer[i+Conf.numProc]);
              doWork(instructionBuffer[i], instructionBuffer[i+Conf.numProc], LSBuffer[i], cycleBuffer[i]);

              //if is a STORE, free mutex
              if(((instructionBuffer[i] >> 56) & 0x00FF) == 1){
                // printf("Done with Store, setting it free!!\n");
                //set flag for this processor so thread waits for STORE to happen
                atomic_flag_clear(PIM_store_flag+i);
              }

              instructionBuffer[i] = 0;
              instructionBuffer[i+Conf.numProc] = 0;
        }
      }
		}

    return 0;
}

void SimulatorCall(void){

  uint64_t stop_t;
  RDPMCStop_tmp(&stop_t);

  int p_core = sched_getcpu();//actual core ID - BENCH_CORE - PIM_CORE

  char t_core = ConfigGetCoreIndex(p_core);

  // int t_core = p_core - 1;

  // printf("Call: Thread Core = %d\n", p_core);
  // printf("CHECK  %d\n", p_core);
  RDPMCAccum_tmp(&stop_t,p_core);

  RDPMCPrint("Call: %ld -> Accum = %ld\n",(Count[p_core].stop - Count[p_core].resume),Count[p_core].accum);

  volatile uint64_t temp_ins0;
  volatile uint64_t temp_ins1;
  volatile uint64_t temp_ls;

  temp_ins0 = PIM_ADDR[t_core*2];
  temp_ins1 = PIM_ADDR[t_core*2+1];
  temp_ls = PIM_LS[t_core];

  // printf("SIM: Instruction = 0x%016lx 0x%016lx at address 0x%016lx\n",temp_ins0, temp_ins1, temp_ls);
  // printf("SIM: DUDE = %d %d %d\n",t_core*2, t_core*2+1, t_core);
  char FIFOfull = 0;

  //check if is a STORE
  if(((temp_ins0 >> 56) & 0x00FF) == 1){
    // printf("Is Store, will wait\n");
    //set flag for this processor
    atomic_flag_test_and_set(PIM_store_flag+t_core);
  }

  do{
      while(atomic_flag_test_and_set(FIFO_flag+t_core))
        ;//Loop while FIFO_flag is locked (1), if unlocked acquire and end Loop
        FIFOfull = BufferAppend(temp_ins0, temp_ins1, temp_ls, stop_t, t_core);
      atomic_flag_clear(FIFO_flag+t_core);//unlock FIFO_flag
  }while(FIFOfull);// Check if FIFOfull, if yes, retry until appended

  //if flag is set, wait until it unsets
  while (atomic_flag_test_and_set(PIM_store_flag+t_core)){
      // printf("Waiting for store!!\n");
  }
    //;//Loop while waiting for STORE to be ready



  atomic_flag_clear(PIM_store_flag+t_core);

  //RESUME COUNTER
  RDPMCStart(p_core);
}
