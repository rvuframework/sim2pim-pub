#include "wrapper.h"

int WrapperCreate(pthread_t *thread, const pthread_attr_t *attr,
                          void *(*start_routine) (void *), void *arg){

    RDPMCStop(BENCH_CORE);
    RDPMCAccum(BENCH_CORE);
    last_counted = 0;

		RDPMCPrint("Bench: %ld - %ld = %ld -> Accum = %ld\n",Count[BENCH_CORE].stop , Count[BENCH_CORE].resume,(Count[BENCH_CORE].stop - Count[BENCH_CORE].resume),Count[BENCH_CORE].accum);

    char p_core = 255;
    int t_core;

    for (t_core = 0; t_core < Conf.numProc; t_core++) {
		    if (Count[t_core].occup == 0) {
          p_core = ConfigToggle_Core(t_core);
          // printf("Core %d free, pining new thread\n", p_core);
          break;
        }
    }

    cpu_set_t masks;
    CPU_ZERO(&masks);
    CPU_SET(p_core, &masks);
    if(pthread_attr_setaffinity_np((pthread_attr_t *)attr, sizeof(cpu_set_t), &masks)){
      fprintf(stderr, "Error: Could not pin thread to core %d\n", p_core);
      return 0;
    }

    thread_arg* t_args= malloc(sizeof(*t_args));
    t_args->arg = arg;
    t_args->start_routine = start_routine;
    t_args->core = t_core;

    RDPMCStart(BENCH_CORE);

		pthread_create(thread, attr, WrapperThread, t_args);

    ////
    //This whole instrumentation section is questionable \/

    RDPMCStop(BENCH_CORE);

    RDPMCAccum(BENCH_CORE);

		RDPMCPrint("Create: %ld -> Accum = %ld\n",Count[BENCH_CORE].resume - Count[BENCH_CORE].stop,Count[BENCH_CORE].accum);

    RDPMCStart(BENCH_CORE);


    return 0;

}

int WrapperJoin(pthread_t thread, void **retval){

	// Do busy waiting for sync with thread
	//
  RDPMCStop(BENCH_CORE);
  RDPMCAccum(BENCH_CORE);
  printf("Join: %ld - %ld = %ld -> Accum = %ld\n",Count[BENCH_CORE].stop, Count[BENCH_CORE].resume,(Count[BENCH_CORE].stop - Count[BENCH_CORE].resume),Count[BENCH_CORE].accum);

	pthread_join(thread, retval);
  if (Conf.enCounter)
  {
    //check if is last join
    if (!last_counted) {
      char all_done = 0;
      uint64_t biggest = 0;
      for (int i = 2; i < Conf.numProc; i++) {
        if (Count[i].occup) {
          all_done++;
        }
        if (Count[i].accum > biggest) {
          biggest = Count[i].accum;
        }
      }
      if (!all_done) {
        printf("Join: biggest = %ld\n",biggest);
      }

      Count[BENCH_CORE].accum += biggest;
      last_counted = 1;
    }

    RDPMCStart(BENCH_CORE);
  }
}

/*
This function is spawned at a new core with pthread_create
It functions to start the counter and receive the function arguments
At the end of the execution, the thread count ends.

*/
void *WrapperThread(void *args){



  struct thread_args *struct_args = args;

	void *(*t_routine)(void *);
  t_routine = struct_args->start_routine;

  void *t_args = struct_args->arg;

  int t_core = struct_args->core;

  void *(*return_arg);


	uint64_t reume_l, stop_l;

  RDPMCStart(t_core);
  reume_l = Count[t_core].resume;

	//For now we assume no return value and no exit at thread, it just ends.
  t_routine(t_args);

  RDPMCStop(t_core);
  RDPMCAccum(t_core);

  ConfigToggle_Core(t_core);

	// accum thread accumCounter to bench accumCounter
	//resume bench counter

  pthread_exit(return_arg);

}
