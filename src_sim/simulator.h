#ifndef SIMULATOR
#define SIMULATOR
#define _GNU_SOURCE

#include <sched.h>
#include <stdatomic.h>
#include <emmintrin.h>
#include <stdalign.h>
#include <time.h>

#include "rdpmc.h"
#include "config.h"
#include "dispatcher.h"
#include "buffer.h"

#define force_inline __attribute__((always_inline))

///////////////////////////////////////////////////////////////////////////////
// __m128i PIM_MAP_ADDR; //PIM MAPPED MEMORY ADDRESS --- THIS SHOULD BE A POINTER
volatile uint64_t PIM_ADDR[64] __attribute__((aligned(16))); //A RANDOM PIM MAPPED MEMORY ADDRESS
volatile uint64_t PIM_LS[32];

extern volatile uint64_t **pim_FIFO_Addr;

volatile atomic_flag *FIFO_flag;
volatile atomic_flag *PIM_start_flag;
volatile atomic_flag *PIM_store_flag;

extern volatile char pimReady;


void* SimulatorPIM(void* args);
void SimulatorCall(void);

#endif
