/*
TODO LIST:

  - Contabilizar os overheads da instrumentação corretamente
    - Parametrizar essas variáveis
    - Fazer o desconto apropriado (no fim?)
  - Criar leitura de arquivo config
  - Recriar biblioteca do buffer e estrutura de dados (pegar do tutorial)
  - Revisar estilo de todos os arquivos

  - Documentar todas as funções e estruturas
  - Fazer contadores para AMD
  - Refazer contadores programaveis Intel

  - Tentar parametrizar os argumentos do doWork para os tipos possíveis de PIM.
    - E.g. numero de canais de memória, tamanho dos buffers, sei lá.
*/

#define _GNU_SOURCE
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <x86intrin.h>

#include "rdpmc.h"
#include "config.h"
#include "benchmark.h"
#include "simulator.h"

volatile char pimReady = 1;
volatile uint64_t **pim_FIFO_Addr;

extern Counters *Count;
extern Config Conf;

extern volatile atomic_flag *FIFO_flag;
extern volatile atomic_flag *PIM_start_flag;

int main(int argc, char *argv[]) {

    if (argc < 2) {
      ConfigHelp();
      Conf.enCounter = 0;
    }else{
      char *a = argv[1];
      Conf.counter = atoi(a);
    }

    ConfigRead();

    ConfigCPU_info();

    // Set the main thread to run in CPU BENCH_CORE
    char p_core = ConfigToggle_Core(BENCH_CORE);

    ConfigSet_instrumentation(p_core);

    cpu_set_t mask;
    CPU_ZERO(&mask);
    CPU_SET(p_core, &mask);

    if (sched_setaffinity(0, sizeof(cpu_set_t), &mask) == -1) {
        fprintf(stderr, "Error: Could not pin Launcher to core %d\n", p_core);
        return 1;
    }

    printf("################################################\n\n");

    //Create Simulator Thread
    pthread_t threadSim;
    pthread_attr_t attr;
    pthread_attr_init(&attr);

    p_core = ConfigToggle_Core(PIM_CORE);

    ////////////////////////////////////////////////////////////////////////////
    cpu_set_t masks;
    CPU_ZERO(&masks);
    CPU_SET(p_core, &masks);
    if(pthread_attr_setaffinity_np(&attr, sizeof(cpu_set_t), &masks)){
      fprintf(stderr, "Error: Could not pin thread to core %d\n", p_core);
      return 0;
    }

    printf("Creating Thread\n");

    //Activates PIM Thread
    atomic_flag_test_and_set(PIM_start_flag);

    pthread_create(&threadSim, &attr, SimulatorPIM, NULL); // Call Thread

    // Here Should do RDPMCWarmup
    RDPMCWarmup();
    Count[BENCH_CORE].accum = 0;

    printf("Main SimulatorPIM core = %d\n", sched_getcpu());

    // printf("Main: CPU: %d\n", sched_getcpu());

    RDPMCStart(BENCH_CORE);
    //=== benchmark + simulation ===//

    int return_benchmark = vecsum();

    //=== benchmark + simulation ===//
    RDPMCStop(BENCH_CORE);
    RDPMCAccum(BENCH_CORE);

    printf("========== Return from Benchmark = %d ==========\n\n",return_benchmark);

    //kills PIM thread
    atomic_flag_clear(PIM_start_flag);
    // pthread_join(threadSim, NULL);
    // while(1);
    RDPMCPrint("Main: %ld \n", Count[BENCH_CORE].accum);

    free(Count);
    for (size_t i = 0; i < Conf.numProc; i++) {
      free(pim_FIFO_Addr[i]);
    }
    free(pim_FIFO_Addr);

    return 0;
}
