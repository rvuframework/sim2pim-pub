#ifndef SIM_CONFIGURE
#define SIM_CONFIGURE

#define _GNU_SOURCE
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <x86intrin.h>
#include <stdatomic.h>

#include "common.h"
#include "rdpmc.h"

#define BENCH_CORE 1
#define PIM_CORE 0
#define MAX_THREAD 2
#define MAX_FIFO 32

// struct pfc_config pfc_configs[18] = {{0}};
struct pfc_config pfc_configs[18];
/** SIMULATION TEST ENVIRONMENT **/
// #define PIM_ADDR 0x0

Counters *Count;
Config Conf;
// counters *counter;
extern volatile uint64_t **pim_FIFO_Addr;
extern volatile atomic_flag *FIFO_flag;
extern volatile atomic_flag *PIM_store_flag;
extern volatile atomic_flag *PIM_start_flag;

char ConfigToggle_Core(char logical_core);
char ConfigGetCoreIndex(char physical_core);
void ConfigHelp(void);
void ConfigCPU_info(void);
void ConfigRead(void);
void ConfigSet_instrumentation(char p_core);

#endif /* end of include guard:  */
