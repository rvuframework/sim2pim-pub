#ifndef BUFFER
#define BUFFER

#include <inttypes.h>
#include "config.h"

int BufferAppend(uint64_t ins0, uint64_t ins1, uint64_t ls, uint64_t cycle, int t_core);
int BufferConsume(uint64_t* instructionBuffer, uint64_t* LSBuffer, uint64_t* cycleBuffer, int t_core);

#endif
