RDPMC=1
#########################################
# This makefile does not consider the parser!
#########################################

#MODIFY PIM DIRECTORY PATH HERE
PIMDIR=./src_pim/rvu_src
PIMINC=$(PIMDIR)/includes

#VPATH=./src/:$(PIMDIR):./benchmark/src
APPDIR=./benchmark
OBJDIR=./obj/
PIMODIR=$(PIMDIR)/obj/
APPODIR=$(APPDIR)/obj/
EXEC=sim2pim

#COMPILE PARAMETERS
CC=gcc
VECTOR= -mavx
CFLAGS= -O3 -no-pie -fno-PIE $(VECTOR)
COMMON= -I$(PIMINC)/ -Isrc_sim/
LDFLAGS=-pthread

#DO NOT TOUCH!
SIMOBJ=config.o main.o simulator.o buffer.o wrapper.o

ifeq ($(RDPMC), 1)
SIMOBJ+=rdpmc.o
endif

#MODIFY PIM DESCRIPTION OBJECT FILES HERE
PIMOBJ=dispatcher.o instruction_manager.o log_generator.o rvu_exec_units.o rvu.o

#MODIFY APP OBJECT FILES HERE
APPOBJ=x86_benchmark_1T_128MB_8192B.o
###### LIST OF AVAILABLE EXAMPLES ######
#x86_benchmark_1T_8kB_4B.o -> x86 scalar
#x86_benchmark_1T_8kB_8B.o -> x86 SSE
#x86_benchmark_1T_8kB_16B.o -> x86 SSE
#x86_benchmark_1T_8kB_128B.o -> RVU 128B
#x86_benchmark_1T_8kB_256B.o -> RVU 256B
#x86_benchmark_1T_8kB_512B.o -> RVU 512B
#x86_benchmark_1T_8kB_1024B.o -> RVU 1024B
#x86_benchmark_1T_8kB_2048B.o -> RVU 2048B
#x86_benchmark_1T_8kB_4096B.o -> RVU 4096B
#x86_benchmark_1T_8kB_8192B.o -> RVU 8192B
#x86_benchmark_1T_128MB_4B.o -> x86 scalar
#x86_benchmark_1T_128MB_8B.o -> x86 SSE
#x86_benchmark_1T_128MB_16B.o -> x86 SSE
#x86_benchmark_1T_128MB_128B.o -> RVU 128B
#x86_benchmark_1T_128MB_256B.o -> RVU 256B
#x86_benchmark_1T_128MB_512B.o -> RVU 512B
#x86_benchmark_1T_128MB_1024B.o -> RVU 1024B
#x86_benchmark_1T_128MB_2048B.o -> RVU 2048B
#x86_benchmark_1T_128MB_4096B.o -> RVU 4096B
#x86_benchmark_1T_128MB_8192B.o -> RVU 8192B




EXECOBJ = $(addprefix $(OBJDIR), $(SIMOBJ))
HWOBJ = $(addprefix $(PIMODIR), $(PIMOBJ))
SWOBJ = $(addprefix $(APPODIR), $(APPOBJ))

DEPS = $(wildcard src_sim/*.h) $(PIMINC)/dispatcher.h
PIMDEPS = $(wildcard $(PIMINC)/*.h)

all: obj pimobj results $(EXEC)

#LINK THE WHOLE SIM2PIM PROGRAM
$(EXEC): $(EXECOBJ) $(HWOBJ) $(SWOBJ)
	$(info 1)
	$(CC) $(COMMON) $(CFLAGS) $^ -o $@ $(LDFLAGS)

#CREATE SIMULATOR OBJECTS
$(OBJDIR)%.o: src_sim/%.c $(DEPS)
	$(info 2)
	$(CC) $(COMMON) $(CFLAGS) -c $< -o $@

#CREATE PIM DESCRIPTION OBJECTS
$(PIMODIR)%.o: $(PIMDIR)/%.c $(PIMDEPS)
	$(info 3)
	$(CC) $(COMMON) $(CFLAGS) -c $< -o $@

#CREATE APPLICATION OBJECTS
$(APPODIR)%.o: $(APPDIR)/src/%.s
	$(info 4)
	$(CC) $(COMMON) $(CFLAGS) -c $< -o $@

obj:
	mkdir -p obj

pimobj:
	mkdir -p $(PIMDIR)/obj

results:
	mkdir -p results

.PHONY: clean

clean:
	rm -rf $(PIMODIR)/*.o $(APPODIR)/*.o $(OBJDIR)/*.o $(EXEC)

#  vecsum_8kB_noprint_VPUOFF_RVU8192B.s ./src/dispatcher.c ./src/rvu.c ./src/rdpmc.c ./src/simp_main.c -o thread.exe

# gcc -S src_sim/main.c -o src_sim/main.s
# gcc -S src_sim/threads.c -Isrc_sim/includes -o src_sim/threads.s
# gcc -S src_sim/wrapper.c -Isrc_sim/includes -Iutils/src/includes -o src_sim/wrapper.s


# run_sim.exe: $(OBJ_B) $(OBJ_U) $(OBJ_P) $(OBJ_S)
# 	@if $(CC) -o $@ $^ $(CFLAGS_S) $(LIB_S); then \
#             echo "\n\e[32m==>Simulator: compilation done \e[0m\n"; \
# 						echo "   run_sim generated\n"; \
#          else \
#             echo "\n\e[31m!!!Simulator: compilation failed \e[0m\n"; \
#          fi
