#rvu per cores for MT = 32
	.text
	.file	"../../benchmark/compiled/IR/tempFile_o3_2.ll"
	.globl	vecsum
	.p2align	4, 0x90
	.type	vecsum,@function
vecsum:                                 # @vecsum
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	xorl	%ecx, %ecx              # encoding: [0x31,0xc9]
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ecx, B(,%rcx,4)        # encoding: [0x89,0x0c,0x8d,A,A,A,A]
                                        #   fixup A - offset: 3, value: B, kind: reloc_signed_4byte
	movl	%eax, C(,%rcx,4)        # encoding: [0x89,0x04,0x8d,A,A,A,A]
                                        #   fixup A - offset: 3, value: C, kind: reloc_signed_4byte
	incq	%rcx                    # encoding: [0x48,0xff,0xc1]
	addl	$2, %eax                # encoding: [0x83,0xc0,0x02]
	cmpq	$2097152, %rcx          # encoding: [0x48,0x81,0xf9,0x00,0x00,0x20,0x00]
                                        # imm = 0x200000
	jne	.LBB0_1                 # encoding: [0x75,A]
                                        #   fixup A - offset: 1, value: .LBB0_1-1, kind: FK_PCRel_1
# BB#2:                                 # %.preheader.preheader
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	clflush	B(%rax)                 # encoding: [0x0f,0xae,0xb8,A,A,A,A]
                                        #   fixup A - offset: 3, value: B, kind: reloc_signed_4byte
	clflush	C(%rax)                 # encoding: [0x0f,0xae,0xb8,A,A,A,A]
                                        #   fixup A - offset: 3, value: C, kind: reloc_signed_4byte
	addq	$4, %rax                # encoding: [0x48,0x83,0xc0,0x04]
	cmpq	$8388608, %rax          # encoding: [0x48,0x3d,0x00,0x00,0x80,0x00]
                                        # imm = 0x800000
	jne	.LBB0_3                 # encoding: [0x75,A]
                                        #   fixup A - offset: 1, value: .LBB0_3-1, kind: FK_PCRel_1
# BB#4:                                 # %min.iters.checked
	mfence                          # encoding: [0x0f,0xae,0xf0]
	movq	$-8388608, %rax         # encoding: [0x48,0xc7,0xc0,0x00,0x00,0x80,0xff]
                                        # imm = 0xFF800000
	.p2align	4, 0x90
.LBB0_5:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	vmovq	B+8388608(%rax), %xmm0  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8388608, kind: reloc_signed_4byte
                                        # xmm0 = mem[0],zero
	vmovq	B+8388616(%rax), %xmm1  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x88,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8388616, kind: reloc_signed_4byte
                                        # xmm1 = mem[0],zero
	vmovq	B+8388624(%rax), %xmm2  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x90,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8388624, kind: reloc_signed_4byte
                                        # xmm2 = mem[0],zero
	vmovq	B+8388632(%rax), %xmm3  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x98,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8388632, kind: reloc_signed_4byte
                                        # xmm3 = mem[0],zero
	vmovq	C+8388608(%rax), %xmm4  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xa0,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8388608, kind: reloc_signed_4byte
                                        # xmm4 = mem[0],zero
	vmovq	C+8388616(%rax), %xmm5  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xa8,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8388616, kind: reloc_signed_4byte
                                        # xmm5 = mem[0],zero
	vmovq	C+8388624(%rax), %xmm6  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xb0,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8388624, kind: reloc_signed_4byte
                                        # xmm6 = mem[0],zero
	vmovq	C+8388632(%rax), %xmm7  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xb8,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8388632, kind: reloc_signed_4byte
                                        # xmm7 = mem[0],zero
	vpaddd	%xmm0, %xmm4, %xmm0     # encoding: [0xc5,0xd9,0xfe,0xc0]
	vmovq	%xmm0, A+8388608(%rax)  # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8388608, kind: reloc_signed_4byte
	vpaddd	%xmm1, %xmm5, %xmm0     # encoding: [0xc5,0xd1,0xfe,0xc1]
	vmovq	%xmm0, A+8388616(%rax)  # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8388616, kind: reloc_signed_4byte
	vpaddd	%xmm2, %xmm6, %xmm0     # encoding: [0xc5,0xc9,0xfe,0xc2]
	vmovq	%xmm0, A+8388624(%rax)  # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8388624, kind: reloc_signed_4byte
	vpaddd	%xmm3, %xmm7, %xmm0     # encoding: [0xc5,0xc1,0xfe,0xc3]
	vmovq	%xmm0, A+8388632(%rax)  # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8388632, kind: reloc_signed_4byte
	vmovq	B+8388640(%rax), %xmm0  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8388640, kind: reloc_signed_4byte
                                        # xmm0 = mem[0],zero
	vmovq	B+8388648(%rax), %xmm1  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x88,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8388648, kind: reloc_signed_4byte
                                        # xmm1 = mem[0],zero
	vmovq	B+8388656(%rax), %xmm2  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x90,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8388656, kind: reloc_signed_4byte
                                        # xmm2 = mem[0],zero
	vmovq	B+8388664(%rax), %xmm3  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0x98,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8388664, kind: reloc_signed_4byte
                                        # xmm3 = mem[0],zero
	vmovq	C+8388640(%rax), %xmm4  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xa0,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8388640, kind: reloc_signed_4byte
                                        # xmm4 = mem[0],zero
	vmovq	C+8388648(%rax), %xmm5  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xa8,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8388648, kind: reloc_signed_4byte
                                        # xmm5 = mem[0],zero
	vmovq	C+8388656(%rax), %xmm6  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xb0,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8388656, kind: reloc_signed_4byte
                                        # xmm6 = mem[0],zero
	vmovq	C+8388664(%rax), %xmm7  # EVEX TO VEX Compression encoding: [0xc5,0xfa,0x7e,0xb8,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8388664, kind: reloc_signed_4byte
                                        # xmm7 = mem[0],zero
	vpaddd	%xmm0, %xmm4, %xmm0     # encoding: [0xc5,0xd9,0xfe,0xc0]
	vmovq	%xmm0, A+8388640(%rax)  # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8388640, kind: reloc_signed_4byte
	vpaddd	%xmm1, %xmm5, %xmm0     # encoding: [0xc5,0xd1,0xfe,0xc1]
	vmovq	%xmm0, A+8388648(%rax)  # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8388648, kind: reloc_signed_4byte
	vpaddd	%xmm2, %xmm6, %xmm0     # encoding: [0xc5,0xc9,0xfe,0xc2]
	vmovq	%xmm0, A+8388656(%rax)  # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8388656, kind: reloc_signed_4byte
	vpaddd	%xmm3, %xmm7, %xmm0     # encoding: [0xc5,0xc1,0xfe,0xc3]
	vmovq	%xmm0, A+8388664(%rax)  # EVEX TO VEX Compression encoding: [0xc5,0xf9,0xd6,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8388664, kind: reloc_signed_4byte
	addq	$64, %rax               # encoding: [0x48,0x83,0xc0,0x40]
	jne	.LBB0_5                 # encoding: [0x75,A]
                                        #   fixup A - offset: 1, value: .LBB0_5-1, kind: FK_PCRel_1
# BB#6:                                 # %middle.block
	pushq	%rax                    # encoding: [0x50]
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	A+4194304(%rip), %esi   # encoding: [0x8b,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4194304)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %esi           # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8388604(%rip), %esi   # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8388604)-4, kind: reloc_riprel_4byte
	movl	$.L.str, %edi           # encoding: [0xbf,A,A,A,A]
                                        #   fixup A - offset: 1, value: .L.str, kind: FK_Data_4
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	callq	printf                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: printf-4, kind: FK_PCRel_4
	movl	A+4194304(%rip), %eax   # encoding: [0x8b,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4194304)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %eax           # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8388604(%rip), %eax   # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8388604)-4, kind: reloc_riprel_4byte
	popq	%rcx                    # encoding: [0x59]
	retq                            # encoding: [0xc3]
.Lfunc_end0:
	.size	vecsum, .Lfunc_end0-vecsum
	.cfi_endproc

	.type	B,@object               # @B
	.comm	B,8388608,64
	.type	C,@object               # @C
	.comm	C,8388608,64
	.type	A,@object               # @A
	.comm	A,8388608,64
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Result = %d\n"
	.size	.L.str, 13

	.globl	PIM_ADDR
	.data
	.align 16
	.type	PIM_ADDR,@object      # @PIM_ADDR
	.size	PIM_ADDR, 512


	.globl	PIM_LS
	.data
	.align 16
	.type	PIM_LS,@object      # @PIM_LS
	.size	PIM_LS, 512


	.section	".note.GNU-stack","",@progbits
