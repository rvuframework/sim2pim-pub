#rvu per cores for MT = 32
	.text
	.file	"../../benchmark/compiled/IR/tempFile_o3_1024.ll"
	.globl	vecsum
	.p2align	4, 0x90
	.type	vecsum,@function
vecsum:                                 # @vecsum
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	pushq	%rax                    # encoding: [0x50]
.Lcfi0:
	.cfi_def_cfa_offset 16
    ###	PIM_4096B_LOAD_DWORD	B(%rip), %RVU_0_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_R32Kb_0 # encoding: [0x61,0x00,0x4a,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0052000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: B-4, kind: reloc_riprel_4byte
    ###	PIM_4096B_LOAD_DWORD	C(%rip), %RVU_0_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_R32Kb_1 # encoding: [0x61,0x00,0x4a,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0052000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: C-4, kind: reloc_riprel_4byte
    ###	PIM_4096B_VADD_DWORD	%RVU_0_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_R32Kb_1, %RVU_0_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_R32Kb_0, %RVU_0_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_R32Kb_0 # encoding: [0x61,0x02,0x4a,0x00,0x00,0x00,0x00,0x04,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0252000000000400 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_4096B_STORE_DWORD	%RVU_0_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_R32Kb_0, A(%rip) # encoding: [0x61,0x01,0x4a,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0152000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: A-4, kind: reloc_riprel_4byte
    ###	PIM_4096B_LOAD_DWORD	B+4096(%rip), %RVU_0_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_R32Kb_0 # encoding: [0x61,0x00,0x4a,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0052000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq B+4096(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (B+4096)-4, kind: reloc_riprel_4byte
    ###	PIM_4096B_LOAD_DWORD	C+4096(%rip), %RVU_0_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_R32Kb_1 # encoding: [0x61,0x00,0x4a,0x00,0x00,0x26,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0052000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq C+4096(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
                                        #   fixup A - offset: 7, value: (C+4096)-4, kind: reloc_riprel_4byte
    ###	PIM_4096B_VADD_DWORD	%RVU_0_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_R32Kb_1, %RVU_0_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_R32Kb_0, %RVU_0_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_R32Kb_0 # encoding: [0x61,0x02,0x4a,0x00,0x00,0x00,0x00,0x04,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0252000000000400 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_4096B_STORE_DWORD	%RVU_0_1_2_3_4_5_6_7_8_9_10_11_12_13_14_15_R32Kb_0, A+4096(%rip) # encoding: [0x61,0x01,0x4a,0x00,0x00,0x06,0x80,A,A,A,A]
    pushq %rax
    mfence
    movq	$0x0152000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    leaq A+4096(%rip),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
                                        #   fixup A - offset: 7, value: (A+4096)-4, kind: reloc_riprel_4byte
	movl	A+4096(%rip), %esi      # encoding: [0x8b,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4096)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %esi           # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8188(%rip), %esi      # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8188)-4, kind: reloc_riprel_4byte
	movl	$.L.str, %edi           # encoding: [0xbf,A,A,A,A]
                                        #   fixup A - offset: 1, value: .L.str, kind: FK_Data_4
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	callq	printf                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: printf-4, kind: FK_PCRel_4
	movl	A+4096(%rip), %eax      # encoding: [0x8b,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4096)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %eax           # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8188(%rip), %eax      # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8188)-4, kind: reloc_riprel_4byte
	popq	%rcx                    # encoding: [0x59]
	retq                            # encoding: [0xc3]
.Lfunc_end0:
	.size	vecsum, .Lfunc_end0-vecsum
	.cfi_endproc

	.type	B,@object               # @B
	.comm	B,8192,64
	.type	C,@object               # @C
	.comm	C,8192,64
	.type	A,@object               # @A
	.comm	A,8192,64
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Result = %d\n"
	.size	.L.str, 13

	.globl	PIM_ADDR
	.data
	.align 16
	.type	PIM_ADDR,@object      # @PIM_ADDR
	.size	PIM_ADDR, 512


	.globl	PIM_LS
	.data
	.align 16
	.type	PIM_LS,@object      # @PIM_LS
	.size	PIM_LS, 512


	.section	".note.GNU-stack","",@progbits
