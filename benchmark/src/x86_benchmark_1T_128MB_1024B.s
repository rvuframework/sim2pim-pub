#rvu per cores for MT = 32
	.text
	.file	"../../benchmark/compiled/IR/tempFile_o3_256.ll"
	.globl	vecsum
	.p2align	4, 0x90
	.type	vecsum,@function
vecsum:                                 # @vecsum
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	pushq	%rbp                    # encoding: [0x55]
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15                    # encoding: [0x41,0x57]
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14                    # encoding: [0x41,0x56]
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx                    # encoding: [0x53]
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax                    # encoding: [0x50]
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	$134217728, %edi        # encoding: [0xbf,0x00,0x00,0x00,0x08]
                                        # imm = 0x8000000
	callq	malloc                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: malloc-4, kind: FK_PCRel_4
	movq	%rax, %r14              # encoding: [0x49,0x89,0xc6]
	movl	$134217728, %edi        # encoding: [0xbf,0x00,0x00,0x00,0x08]
                                        # imm = 0x8000000
	callq	malloc                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: malloc-4, kind: FK_PCRel_4
	movq	%rax, %r15              # encoding: [0x49,0x89,0xc7]
	movl	$134217728, %edi        # encoding: [0xbf,0x00,0x00,0x00,0x08]
                                        # imm = 0x8000000
	callq	malloc                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: malloc-4, kind: FK_PCRel_4
	movq	%rax, %rbx              # encoding: [0x48,0x89,0xc3]
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	.p2align	4, 0x90
.LBB0_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
    ###	PIM_1024B_LOAD_DWORD	(%r15,%rax), %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x00,0x88,0x00,0x00,0x07,0x80,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq (%r15,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_LOAD_DWORD	(%rbx,%rax), %RVU_0_1_2_3_R8Kb_1 # encoding: [0x61,0x00,0x88,0x00,0x00,0x21,0x80,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq (%rbx,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_VADD_DWORD	%RVU_0_1_2_3_R8Kb_1, %RVU_0_1_2_3_R8Kb_0, %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x02,0x48,0x00,0x00,0x00,0x00,0x04,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0242000000000400, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_1024B_STORE_DWORD	%RVU_0_1_2_3_R8Kb_0, (%r14,%rax) # encoding: [0x61,0x01,0x88,0x00,0x00,0x07,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0142000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq (%r14,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
    ###	PIM_1024B_LOAD_DWORD	1024(%r15,%rax), %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x00,0x88,0x00,0x00,0x07,0x80,0x01,0x00,0x00,0x00,0x00,0x04,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 1024(%r15,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_LOAD_DWORD	1024(%rbx,%rax), %RVU_0_1_2_3_R8Kb_1 # encoding: [0x61,0x00,0x88,0x00,0x00,0x21,0x80,0x01,0x00,0x00,0x00,0x00,0x04,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 1024(%rbx,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_VADD_DWORD	%RVU_0_1_2_3_R8Kb_1, %RVU_0_1_2_3_R8Kb_0, %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x02,0x48,0x00,0x00,0x00,0x00,0x04,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0242000000000400, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_1024B_STORE_DWORD	%RVU_0_1_2_3_R8Kb_0, 1024(%r14,%rax) # encoding: [0x61,0x01,0x88,0x00,0x00,0x07,0x00,0x01,0x00,0x00,0x00,0x00,0x04,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0142000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 1024(%r14,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
    ###	PIM_1024B_LOAD_DWORD	2048(%r15,%rax), %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x00,0x88,0x00,0x00,0x07,0x80,0x01,0x00,0x00,0x00,0x00,0x08,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 2048(%r15,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_LOAD_DWORD	2048(%rbx,%rax), %RVU_0_1_2_3_R8Kb_1 # encoding: [0x61,0x00,0x88,0x00,0x00,0x21,0x80,0x01,0x00,0x00,0x00,0x00,0x08,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 2048(%rbx,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_VADD_DWORD	%RVU_0_1_2_3_R8Kb_1, %RVU_0_1_2_3_R8Kb_0, %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x02,0x48,0x00,0x00,0x00,0x00,0x04,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0242000000000400, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_1024B_STORE_DWORD	%RVU_0_1_2_3_R8Kb_0, 2048(%r14,%rax) # encoding: [0x61,0x01,0x88,0x00,0x00,0x07,0x00,0x01,0x00,0x00,0x00,0x00,0x08,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0142000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 2048(%r14,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
    ###	PIM_1024B_LOAD_DWORD	3072(%r15,%rax), %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x00,0x88,0x00,0x00,0x07,0x80,0x01,0x00,0x00,0x00,0x00,0x0c,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 3072(%r15,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_LOAD_DWORD	3072(%rbx,%rax), %RVU_0_1_2_3_R8Kb_1 # encoding: [0x61,0x00,0x88,0x00,0x00,0x21,0x80,0x01,0x00,0x00,0x00,0x00,0x0c,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 3072(%rbx,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_VADD_DWORD	%RVU_0_1_2_3_R8Kb_1, %RVU_0_1_2_3_R8Kb_0, %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x02,0x48,0x00,0x00,0x00,0x00,0x04,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0242000000000400, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_1024B_STORE_DWORD	%RVU_0_1_2_3_R8Kb_0, 3072(%r14,%rax) # encoding: [0x61,0x01,0x88,0x00,0x00,0x07,0x00,0x01,0x00,0x00,0x00,0x00,0x0c,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0142000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 3072(%r14,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
    ###	PIM_1024B_LOAD_DWORD	4096(%r15,%rax), %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x00,0x88,0x00,0x00,0x07,0x80,0x01,0x00,0x00,0x00,0x00,0x10,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 4096(%r15,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_LOAD_DWORD	4096(%rbx,%rax), %RVU_0_1_2_3_R8Kb_1 # encoding: [0x61,0x00,0x88,0x00,0x00,0x21,0x80,0x01,0x00,0x00,0x00,0x00,0x10,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 4096(%rbx,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_VADD_DWORD	%RVU_0_1_2_3_R8Kb_1, %RVU_0_1_2_3_R8Kb_0, %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x02,0x48,0x00,0x00,0x00,0x00,0x04,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0242000000000400, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_1024B_STORE_DWORD	%RVU_0_1_2_3_R8Kb_0, 4096(%r14,%rax) # encoding: [0x61,0x01,0x88,0x00,0x00,0x07,0x00,0x01,0x00,0x00,0x00,0x00,0x10,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0142000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 4096(%r14,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
    ###	PIM_1024B_LOAD_DWORD	5120(%r15,%rax), %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x00,0x88,0x00,0x00,0x07,0x80,0x01,0x00,0x00,0x00,0x00,0x14,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 5120(%r15,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_LOAD_DWORD	5120(%rbx,%rax), %RVU_0_1_2_3_R8Kb_1 # encoding: [0x61,0x00,0x88,0x00,0x00,0x21,0x80,0x01,0x00,0x00,0x00,0x00,0x14,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 5120(%rbx,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_VADD_DWORD	%RVU_0_1_2_3_R8Kb_1, %RVU_0_1_2_3_R8Kb_0, %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x02,0x48,0x00,0x00,0x00,0x00,0x04,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0242000000000400, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_1024B_STORE_DWORD	%RVU_0_1_2_3_R8Kb_0, 5120(%r14,%rax) # encoding: [0x61,0x01,0x88,0x00,0x00,0x07,0x00,0x01,0x00,0x00,0x00,0x00,0x14,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0142000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 5120(%r14,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
    ###	PIM_1024B_LOAD_DWORD	6144(%r15,%rax), %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x00,0x88,0x00,0x00,0x07,0x80,0x01,0x00,0x00,0x00,0x00,0x18,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 6144(%r15,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_LOAD_DWORD	6144(%rbx,%rax), %RVU_0_1_2_3_R8Kb_1 # encoding: [0x61,0x00,0x88,0x00,0x00,0x21,0x80,0x01,0x00,0x00,0x00,0x00,0x18,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 6144(%rbx,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_VADD_DWORD	%RVU_0_1_2_3_R8Kb_1, %RVU_0_1_2_3_R8Kb_0, %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x02,0x48,0x00,0x00,0x00,0x00,0x04,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0242000000000400, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_1024B_STORE_DWORD	%RVU_0_1_2_3_R8Kb_0, 6144(%r14,%rax) # encoding: [0x61,0x01,0x88,0x00,0x00,0x07,0x00,0x01,0x00,0x00,0x00,0x00,0x18,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0142000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 6144(%r14,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
    ###	PIM_1024B_LOAD_DWORD	7168(%r15,%rax), %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x00,0x88,0x00,0x00,0x07,0x80,0x01,0x00,0x00,0x00,0x00,0x1c,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000000000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 7168(%r15,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_LOAD_DWORD	7168(%rbx,%rax), %RVU_0_1_2_3_R8Kb_1 # encoding: [0x61,0x00,0x88,0x00,0x00,0x21,0x80,0x01,0x00,0x00,0x00,0x00,0x1c,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0042000020000000 , %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000 , %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 7168(%rbx,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM LOAD ###
    ###	PIM_1024B_VADD_DWORD	%RVU_0_1_2_3_R8Kb_1, %RVU_0_1_2_3_R8Kb_0, %RVU_0_1_2_3_R8Kb_0 # encoding: [0x61,0x02,0x48,0x00,0x00,0x00,0x00,0x04,0x00,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0242000000000400, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    callq SimulatorCall
    #######################
    popq %rax
### END PIM VADD ###
    ###	PIM_1024B_STORE_DWORD	%RVU_0_1_2_3_R8Kb_0, 7168(%r14,%rax) # encoding: [0x61,0x01,0x88,0x00,0x00,0x07,0x00,0x01,0x00,0x00,0x00,0x00,0x1c,0x00,0x00]
    pushq %rax
    mfence
    movq	$0x0142000000000000, %rax
    movq	%rax, %xmm0
    movq	$0x0000000000000000, %rax
    movq	%rax, %xmm1
    punpcklqdq	%xmm1,%xmm0
    movntdq	%xmm0,PIM_ADDR+0(%rip)
    mfence
    #######################
    popq %rax
    pushq %r10
    movq %rax, %r10
    leaq 7168(%r14,%rax),%rax		 #CLFLUSH ADDRESS
    movq	%rax,PIM_LS+0(%rip)
    #######################
    movq %r10, %rax
    popq %r10
    pushq %rax
    callq SimulatorCall
    #######################
    popq %rax
###### SIMULATION CALL ENDS ######
### END PIM STORE ###
	addq	$8192, %rax             # encoding: [0x48,0x05,0x00,0x20,0x00,0x00]
                                        # imm = 0x2000
	cmpq	$134217728, %rax        # encoding: [0x48,0x3d,0x00,0x00,0x00,0x08]
                                        # imm = 0x8000000
	jne	.LBB0_1                 # encoding: [0x75,A]
                                        #   fixup A - offset: 1, value: .LBB0_1-1, kind: FK_PCRel_1
# BB#2:                                 # %middle.block
	movl	67108864(%r14), %ebp    # encoding: [0x41,0x8b,0xae,0x00,0x00,0x00,0x04]
	addl	(%r14), %ebp            # encoding: [0x41,0x03,0x2e]
	addl	134217724(%r14), %ebp   # encoding: [0x41,0x03,0xae,0xfc,0xff,0xff,0x07]
	movl	$.L.str, %edi           # encoding: [0xbf,A,A,A,A]
                                        #   fixup A - offset: 1, value: .L.str, kind: FK_Data_4
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	movl	%ebp, %esi              # encoding: [0x89,0xee]
	callq	printf                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: printf-4, kind: FK_PCRel_4
	movq	%r14, %rdi              # encoding: [0x4c,0x89,0xf7]
	callq	free                    # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: free-4, kind: FK_PCRel_4
	movq	%r15, %rdi              # encoding: [0x4c,0x89,0xff]
	callq	free                    # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: free-4, kind: FK_PCRel_4
	movq	%rbx, %rdi              # encoding: [0x48,0x89,0xdf]
	callq	free                    # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: free-4, kind: FK_PCRel_4
	movl	%ebp, %eax              # encoding: [0x89,0xe8]
	addq	$8, %rsp                # encoding: [0x48,0x83,0xc4,0x08]
	popq	%rbx                    # encoding: [0x5b]
	popq	%r14                    # encoding: [0x41,0x5e]
	popq	%r15                    # encoding: [0x41,0x5f]
	popq	%rbp                    # encoding: [0x5d]
	retq                            # encoding: [0xc3]
.Lfunc_end0:
	.size	vecsum, .Lfunc_end0-vecsum
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Result = %d\n"
	.size	.L.str, 13

	.globl	PIM_ADDR
	.data
	.align 16
	.type	PIM_ADDR,@object      # @PIM_ADDR
	.size	PIM_ADDR, 512


	.globl	PIM_LS
	.data
	.align 16
	.type	PIM_LS,@object      # @PIM_LS
	.size	PIM_LS, 512


	.section	".note.GNU-stack","",@progbits
