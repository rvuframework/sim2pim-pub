#rvu per cores for MT = 32
	.text
	.file	"../../benchmark/compiled/IR/tempFile_o3_4.ll"
	.globl	vecsum
	.p2align	4, 0x90
	.type	vecsum,@function
vecsum:                                 # @vecsum
	.cfi_startproc
# BB#0:                                 # %min.iters.checked
	movq	$-8192, %rax            # encoding: [0x48,0xc7,0xc0,0x00,0xe0,0xff,0xff]
                                        # imm = 0xE000
	.p2align	4, 0x90
.LBB0_1:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	vmovdqa	C+8192(%rax), %xmm0     # encoding: [0xc5,0xf9,0x6f,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8192, kind: reloc_signed_4byte
	vmovdqa	C+8208(%rax), %xmm1     # encoding: [0xc5,0xf9,0x6f,0x88,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8208, kind: reloc_signed_4byte
	vmovdqa	C+8224(%rax), %xmm2     # encoding: [0xc5,0xf9,0x6f,0x90,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8224, kind: reloc_signed_4byte
	vmovdqa	C+8240(%rax), %xmm3     # encoding: [0xc5,0xf9,0x6f,0x98,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8240, kind: reloc_signed_4byte
	vpaddd	B+8192(%rax), %xmm0, %xmm0 # encoding: [0xc5,0xf9,0xfe,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8192, kind: reloc_signed_4byte
	vpaddd	B+8208(%rax), %xmm1, %xmm1 # encoding: [0xc5,0xf1,0xfe,0x88,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8208, kind: reloc_signed_4byte
	vpaddd	B+8224(%rax), %xmm2, %xmm2 # encoding: [0xc5,0xe9,0xfe,0x90,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8224, kind: reloc_signed_4byte
	vpaddd	B+8240(%rax), %xmm3, %xmm3 # encoding: [0xc5,0xe1,0xfe,0x98,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8240, kind: reloc_signed_4byte
	vmovdqa	%xmm0, A+8192(%rax)     # encoding: [0xc5,0xf9,0x7f,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8192, kind: reloc_signed_4byte
	vmovdqa	%xmm1, A+8208(%rax)     # encoding: [0xc5,0xf9,0x7f,0x88,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8208, kind: reloc_signed_4byte
	vmovdqa	%xmm2, A+8224(%rax)     # encoding: [0xc5,0xf9,0x7f,0x90,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8224, kind: reloc_signed_4byte
	vmovdqa	%xmm3, A+8240(%rax)     # encoding: [0xc5,0xf9,0x7f,0x98,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8240, kind: reloc_signed_4byte
	vmovdqa	C+8256(%rax), %xmm0     # encoding: [0xc5,0xf9,0x6f,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8256, kind: reloc_signed_4byte
	vmovdqa	C+8272(%rax), %xmm1     # encoding: [0xc5,0xf9,0x6f,0x88,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8272, kind: reloc_signed_4byte
	vmovdqa	C+8288(%rax), %xmm2     # encoding: [0xc5,0xf9,0x6f,0x90,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8288, kind: reloc_signed_4byte
	vmovdqa	C+8304(%rax), %xmm3     # encoding: [0xc5,0xf9,0x6f,0x98,A,A,A,A]
                                        #   fixup A - offset: 4, value: C+8304, kind: reloc_signed_4byte
	vpaddd	B+8256(%rax), %xmm0, %xmm0 # encoding: [0xc5,0xf9,0xfe,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8256, kind: reloc_signed_4byte
	vpaddd	B+8272(%rax), %xmm1, %xmm1 # encoding: [0xc5,0xf1,0xfe,0x88,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8272, kind: reloc_signed_4byte
	vpaddd	B+8288(%rax), %xmm2, %xmm2 # encoding: [0xc5,0xe9,0xfe,0x90,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8288, kind: reloc_signed_4byte
	vpaddd	B+8304(%rax), %xmm3, %xmm3 # encoding: [0xc5,0xe1,0xfe,0x98,A,A,A,A]
                                        #   fixup A - offset: 4, value: B+8304, kind: reloc_signed_4byte
	vmovdqa	%xmm0, A+8256(%rax)     # encoding: [0xc5,0xf9,0x7f,0x80,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8256, kind: reloc_signed_4byte
	vmovdqa	%xmm1, A+8272(%rax)     # encoding: [0xc5,0xf9,0x7f,0x88,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8272, kind: reloc_signed_4byte
	vmovdqa	%xmm2, A+8288(%rax)     # encoding: [0xc5,0xf9,0x7f,0x90,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8288, kind: reloc_signed_4byte
	vmovdqa	%xmm3, A+8304(%rax)     # encoding: [0xc5,0xf9,0x7f,0x98,A,A,A,A]
                                        #   fixup A - offset: 4, value: A+8304, kind: reloc_signed_4byte
	addq	$128, %rax              # encoding: [0x48,0x05,0x80,0x00,0x00,0x00]
	jne	.LBB0_1                 # encoding: [0x75,A]
                                        #   fixup A - offset: 1, value: .LBB0_1-1, kind: FK_PCRel_1
# BB#2:                                 # %middle.block
	pushq	%rax                    # encoding: [0x50]
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	A+4096(%rip), %esi      # encoding: [0x8b,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4096)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %esi           # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8188(%rip), %esi      # encoding: [0x03,0x35,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8188)-4, kind: reloc_riprel_4byte
	movl	$.L.str, %edi           # encoding: [0xbf,A,A,A,A]
                                        #   fixup A - offset: 1, value: .L.str, kind: FK_Data_4
	xorl	%eax, %eax              # encoding: [0x31,0xc0]
	callq	printf                  # encoding: [0xe8,A,A,A,A]
                                        #   fixup A - offset: 1, value: printf-4, kind: FK_PCRel_4
	movl	A+4096(%rip), %eax      # encoding: [0x8b,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+4096)-4, kind: reloc_riprel_4byte
	addl	A(%rip), %eax           # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: A-4, kind: reloc_riprel_4byte
	addl	A+8188(%rip), %eax      # encoding: [0x03,0x05,A,A,A,A]
                                        #   fixup A - offset: 2, value: (A+8188)-4, kind: reloc_riprel_4byte
	popq	%rcx                    # encoding: [0x59]
	retq                            # encoding: [0xc3]
.Lfunc_end0:
	.size	vecsum, .Lfunc_end0-vecsum
	.cfi_endproc

	.type	B,@object               # @B
	.comm	B,8192,64
	.type	C,@object               # @C
	.comm	C,8192,64
	.type	A,@object               # @A
	.comm	A,8192,64
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Result = %d\n"
	.size	.L.str, 13

	.globl	PIM_ADDR
	.data
	.align 16
	.type	PIM_ADDR,@object      # @PIM_ADDR
	.size	PIM_ADDR, 512


	.globl	PIM_LS
	.data
	.align 16
	.type	PIM_LS,@object      # @PIM_LS
	.size	PIM_LS, 512


	.section	".note.GNU-stack","",@progbits
