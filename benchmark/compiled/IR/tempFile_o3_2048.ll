; ModuleID = '../../benchmark/compiled/IR/tempFile.ll'
source_filename = "../../benchmark/tempFile.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

@.str = private unnamed_addr constant [13 x i8] c"Result = %d\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @vecsum() local_unnamed_addr #0 {
min.iters.checked:
  %0 = tail call noalias i8* @malloc(i64 134217728) #2
  %1 = bitcast i8* %0 to i32*
  %2 = tail call noalias i8* @malloc(i64 134217728) #2
  %3 = bitcast i8* %2 to i32*
  %4 = tail call noalias i8* @malloc(i64 134217728) #2
  %5 = bitcast i8* %4 to i32*
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %min.iters.checked
  %index = phi i64 [ 0, %min.iters.checked ], [ %index.next.7, %vector.body ]
  %6 = getelementptr inbounds i32, i32* %3, i64 %index
  %7 = bitcast i32* %6 to <2048 x i32>*
  %wide.load = load <2048 x i32>, <2048 x i32>* %7, align 4, !tbaa !1
  %8 = getelementptr inbounds i32, i32* %5, i64 %index
  %9 = bitcast i32* %8 to <2048 x i32>*
  %wide.load25 = load <2048 x i32>, <2048 x i32>* %9, align 4, !tbaa !1
  %10 = add <2048 x i32> %wide.load25, %wide.load
  %11 = getelementptr inbounds i32, i32* %1, i64 %index
  %12 = bitcast i32* %11 to <2048 x i32>*
  store <2048 x i32> %10, <2048 x i32>* %12, align 4, !tbaa !1
  %index.next = or i64 %index, 2048
  %13 = getelementptr inbounds i32, i32* %3, i64 %index.next
  %14 = bitcast i32* %13 to <2048 x i32>*
  %wide.load.1 = load <2048 x i32>, <2048 x i32>* %14, align 4, !tbaa !1
  %15 = getelementptr inbounds i32, i32* %5, i64 %index.next
  %16 = bitcast i32* %15 to <2048 x i32>*
  %wide.load25.1 = load <2048 x i32>, <2048 x i32>* %16, align 4, !tbaa !1
  %17 = add <2048 x i32> %wide.load25.1, %wide.load.1
  %18 = getelementptr inbounds i32, i32* %1, i64 %index.next
  %19 = bitcast i32* %18 to <2048 x i32>*
  store <2048 x i32> %17, <2048 x i32>* %19, align 4, !tbaa !1
  %index.next.1 = or i64 %index, 4096
  %20 = getelementptr inbounds i32, i32* %3, i64 %index.next.1
  %21 = bitcast i32* %20 to <2048 x i32>*
  %wide.load.2 = load <2048 x i32>, <2048 x i32>* %21, align 4, !tbaa !1
  %22 = getelementptr inbounds i32, i32* %5, i64 %index.next.1
  %23 = bitcast i32* %22 to <2048 x i32>*
  %wide.load25.2 = load <2048 x i32>, <2048 x i32>* %23, align 4, !tbaa !1
  %24 = add <2048 x i32> %wide.load25.2, %wide.load.2
  %25 = getelementptr inbounds i32, i32* %1, i64 %index.next.1
  %26 = bitcast i32* %25 to <2048 x i32>*
  store <2048 x i32> %24, <2048 x i32>* %26, align 4, !tbaa !1
  %index.next.2 = or i64 %index, 6144
  %27 = getelementptr inbounds i32, i32* %3, i64 %index.next.2
  %28 = bitcast i32* %27 to <2048 x i32>*
  %wide.load.3 = load <2048 x i32>, <2048 x i32>* %28, align 4, !tbaa !1
  %29 = getelementptr inbounds i32, i32* %5, i64 %index.next.2
  %30 = bitcast i32* %29 to <2048 x i32>*
  %wide.load25.3 = load <2048 x i32>, <2048 x i32>* %30, align 4, !tbaa !1
  %31 = add <2048 x i32> %wide.load25.3, %wide.load.3
  %32 = getelementptr inbounds i32, i32* %1, i64 %index.next.2
  %33 = bitcast i32* %32 to <2048 x i32>*
  store <2048 x i32> %31, <2048 x i32>* %33, align 4, !tbaa !1
  %index.next.3 = or i64 %index, 8192
  %34 = getelementptr inbounds i32, i32* %3, i64 %index.next.3
  %35 = bitcast i32* %34 to <2048 x i32>*
  %wide.load.4 = load <2048 x i32>, <2048 x i32>* %35, align 4, !tbaa !1
  %36 = getelementptr inbounds i32, i32* %5, i64 %index.next.3
  %37 = bitcast i32* %36 to <2048 x i32>*
  %wide.load25.4 = load <2048 x i32>, <2048 x i32>* %37, align 4, !tbaa !1
  %38 = add <2048 x i32> %wide.load25.4, %wide.load.4
  %39 = getelementptr inbounds i32, i32* %1, i64 %index.next.3
  %40 = bitcast i32* %39 to <2048 x i32>*
  store <2048 x i32> %38, <2048 x i32>* %40, align 4, !tbaa !1
  %index.next.4 = or i64 %index, 10240
  %41 = getelementptr inbounds i32, i32* %3, i64 %index.next.4
  %42 = bitcast i32* %41 to <2048 x i32>*
  %wide.load.5 = load <2048 x i32>, <2048 x i32>* %42, align 4, !tbaa !1
  %43 = getelementptr inbounds i32, i32* %5, i64 %index.next.4
  %44 = bitcast i32* %43 to <2048 x i32>*
  %wide.load25.5 = load <2048 x i32>, <2048 x i32>* %44, align 4, !tbaa !1
  %45 = add <2048 x i32> %wide.load25.5, %wide.load.5
  %46 = getelementptr inbounds i32, i32* %1, i64 %index.next.4
  %47 = bitcast i32* %46 to <2048 x i32>*
  store <2048 x i32> %45, <2048 x i32>* %47, align 4, !tbaa !1
  %index.next.5 = or i64 %index, 12288
  %48 = getelementptr inbounds i32, i32* %3, i64 %index.next.5
  %49 = bitcast i32* %48 to <2048 x i32>*
  %wide.load.6 = load <2048 x i32>, <2048 x i32>* %49, align 4, !tbaa !1
  %50 = getelementptr inbounds i32, i32* %5, i64 %index.next.5
  %51 = bitcast i32* %50 to <2048 x i32>*
  %wide.load25.6 = load <2048 x i32>, <2048 x i32>* %51, align 4, !tbaa !1
  %52 = add <2048 x i32> %wide.load25.6, %wide.load.6
  %53 = getelementptr inbounds i32, i32* %1, i64 %index.next.5
  %54 = bitcast i32* %53 to <2048 x i32>*
  store <2048 x i32> %52, <2048 x i32>* %54, align 4, !tbaa !1
  %index.next.6 = or i64 %index, 14336
  %55 = getelementptr inbounds i32, i32* %3, i64 %index.next.6
  %56 = bitcast i32* %55 to <2048 x i32>*
  %wide.load.7 = load <2048 x i32>, <2048 x i32>* %56, align 4, !tbaa !1
  %57 = getelementptr inbounds i32, i32* %5, i64 %index.next.6
  %58 = bitcast i32* %57 to <2048 x i32>*
  %wide.load25.7 = load <2048 x i32>, <2048 x i32>* %58, align 4, !tbaa !1
  %59 = add <2048 x i32> %wide.load25.7, %wide.load.7
  %60 = getelementptr inbounds i32, i32* %1, i64 %index.next.6
  %61 = bitcast i32* %60 to <2048 x i32>*
  store <2048 x i32> %59, <2048 x i32>* %61, align 4, !tbaa !1
  %index.next.7 = add nsw i64 %index, 16384
  %62 = icmp eq i64 %index.next.7, 33554432
  br i1 %62, label %middle.block, label %vector.body, !llvm.loop !5

middle.block:                                     ; preds = %vector.body
  %63 = load i32, i32* %1, align 4, !tbaa !1
  %64 = getelementptr inbounds i8, i8* %0, i64 67108864
  %65 = bitcast i8* %64 to i32*
  %66 = load i32, i32* %65, align 4, !tbaa !1
  %67 = add i32 %66, %63
  %68 = getelementptr inbounds i8, i8* %0, i64 134217724
  %69 = bitcast i8* %68 to i32*
  %70 = load i32, i32* %69, align 4, !tbaa !1
  %71 = add i32 %67, %70
  %72 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str, i64 0, i64 0), i32 %71)
  tail call void @free(i8* nonnull %0) #2
  tail call void @free(i8* nonnull %2) #2
  tail call void @free(i8* nonnull %4) #2
  ret i32 %71
}

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) local_unnamed_addr #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) local_unnamed_addr #1

; Function Attrs: nounwind
declare void @free(i8* nocapture) local_unnamed_addr #1

attributes #0 = { nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="knl" "target-features"="+adx,+aes,+avx,+avx2,+avx512cd,+avx512er,+avx512f,+avx512pf,+bmi,+bmi2,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+lzcnt,+mmx,+movbe,+pclmul,+popcnt,+prefetchwt1,+rdrnd,+rdseed,+rtm,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsaveopt" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="knl" "target-features"="+adx,+aes,+avx,+avx2,+avx512cd,+avx512er,+avx512f,+avx512pf,+bmi,+bmi2,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+lzcnt,+mmx,+movbe,+pclmul,+popcnt,+prefetchwt1,+rdrnd,+rdseed,+rtm,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsaveopt" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 4.0.1-10 (tags/RELEASE_401/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = distinct !{!5, !6, !7}
!6 = !{!"llvm.loop.vectorize.width", i32 1}
!7 = !{!"llvm.loop.interleave.count", i32 1}
