; ModuleID = '../../benchmark/compiled/IR/tempFile.ll'
source_filename = "../../benchmark/tempFile.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

@.str = private unnamed_addr constant [13 x i8] c"Result = %d\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @vecsum() local_unnamed_addr #0 {
  %1 = tail call noalias i8* @malloc(i64 134217728) #2
  %2 = bitcast i8* %1 to i32*
  %3 = tail call noalias i8* @malloc(i64 134217728) #2
  %4 = bitcast i8* %3 to i32*
  %5 = tail call noalias i8* @malloc(i64 134217728) #2
  %6 = bitcast i8* %5 to i32*
  br label %18

; <label>:7:                                      ; preds = %18
  %8 = load i32, i32* %2, align 4, !tbaa !1
  %9 = getelementptr inbounds i8, i8* %1, i64 67108864
  %10 = bitcast i8* %9 to i32*
  %11 = load i32, i32* %10, align 4, !tbaa !1
  %12 = add i32 %11, %8
  %13 = getelementptr inbounds i8, i8* %1, i64 134217724
  %14 = bitcast i8* %13 to i32*
  %15 = load i32, i32* %14, align 4, !tbaa !1
  %16 = add i32 %12, %15
  %17 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str, i64 0, i64 0), i32 %16)
  tail call void @free(i8* nonnull %1) #2
  tail call void @free(i8* nonnull %3) #2
  tail call void @free(i8* nonnull %5) #2
  ret i32 %16

; <label>:18:                                     ; preds = %18, %0
  %indvars.iv = phi i64 [ 0, %0 ], [ %indvars.iv.next.7, %18 ]
  %19 = getelementptr inbounds i32, i32* %4, i64 %indvars.iv
  %20 = load i32, i32* %19, align 4, !tbaa !1
  %21 = getelementptr inbounds i32, i32* %6, i64 %indvars.iv
  %22 = load i32, i32* %21, align 4, !tbaa !1
  %23 = add i32 %22, %20
  %24 = getelementptr inbounds i32, i32* %2, i64 %indvars.iv
  store i32 %23, i32* %24, align 4, !tbaa !1
  %indvars.iv.next = or i64 %indvars.iv, 1
  %25 = getelementptr inbounds i32, i32* %4, i64 %indvars.iv.next
  %26 = load i32, i32* %25, align 4, !tbaa !1
  %27 = getelementptr inbounds i32, i32* %6, i64 %indvars.iv.next
  %28 = load i32, i32* %27, align 4, !tbaa !1
  %29 = add i32 %28, %26
  %30 = getelementptr inbounds i32, i32* %2, i64 %indvars.iv.next
  store i32 %29, i32* %30, align 4, !tbaa !1
  %indvars.iv.next.1 = or i64 %indvars.iv, 2
  %31 = getelementptr inbounds i32, i32* %4, i64 %indvars.iv.next.1
  %32 = load i32, i32* %31, align 4, !tbaa !1
  %33 = getelementptr inbounds i32, i32* %6, i64 %indvars.iv.next.1
  %34 = load i32, i32* %33, align 4, !tbaa !1
  %35 = add i32 %34, %32
  %36 = getelementptr inbounds i32, i32* %2, i64 %indvars.iv.next.1
  store i32 %35, i32* %36, align 4, !tbaa !1
  %indvars.iv.next.2 = or i64 %indvars.iv, 3
  %37 = getelementptr inbounds i32, i32* %4, i64 %indvars.iv.next.2
  %38 = load i32, i32* %37, align 4, !tbaa !1
  %39 = getelementptr inbounds i32, i32* %6, i64 %indvars.iv.next.2
  %40 = load i32, i32* %39, align 4, !tbaa !1
  %41 = add i32 %40, %38
  %42 = getelementptr inbounds i32, i32* %2, i64 %indvars.iv.next.2
  store i32 %41, i32* %42, align 4, !tbaa !1
  %indvars.iv.next.3 = or i64 %indvars.iv, 4
  %43 = getelementptr inbounds i32, i32* %4, i64 %indvars.iv.next.3
  %44 = load i32, i32* %43, align 4, !tbaa !1
  %45 = getelementptr inbounds i32, i32* %6, i64 %indvars.iv.next.3
  %46 = load i32, i32* %45, align 4, !tbaa !1
  %47 = add i32 %46, %44
  %48 = getelementptr inbounds i32, i32* %2, i64 %indvars.iv.next.3
  store i32 %47, i32* %48, align 4, !tbaa !1
  %indvars.iv.next.4 = or i64 %indvars.iv, 5
  %49 = getelementptr inbounds i32, i32* %4, i64 %indvars.iv.next.4
  %50 = load i32, i32* %49, align 4, !tbaa !1
  %51 = getelementptr inbounds i32, i32* %6, i64 %indvars.iv.next.4
  %52 = load i32, i32* %51, align 4, !tbaa !1
  %53 = add i32 %52, %50
  %54 = getelementptr inbounds i32, i32* %2, i64 %indvars.iv.next.4
  store i32 %53, i32* %54, align 4, !tbaa !1
  %indvars.iv.next.5 = or i64 %indvars.iv, 6
  %55 = getelementptr inbounds i32, i32* %4, i64 %indvars.iv.next.5
  %56 = load i32, i32* %55, align 4, !tbaa !1
  %57 = getelementptr inbounds i32, i32* %6, i64 %indvars.iv.next.5
  %58 = load i32, i32* %57, align 4, !tbaa !1
  %59 = add i32 %58, %56
  %60 = getelementptr inbounds i32, i32* %2, i64 %indvars.iv.next.5
  store i32 %59, i32* %60, align 4, !tbaa !1
  %indvars.iv.next.6 = or i64 %indvars.iv, 7
  %61 = getelementptr inbounds i32, i32* %4, i64 %indvars.iv.next.6
  %62 = load i32, i32* %61, align 4, !tbaa !1
  %63 = getelementptr inbounds i32, i32* %6, i64 %indvars.iv.next.6
  %64 = load i32, i32* %63, align 4, !tbaa !1
  %65 = add i32 %64, %62
  %66 = getelementptr inbounds i32, i32* %2, i64 %indvars.iv.next.6
  store i32 %65, i32* %66, align 4, !tbaa !1
  %indvars.iv.next.7 = add nsw i64 %indvars.iv, 8
  %exitcond.7 = icmp eq i64 %indvars.iv.next.7, 33554432
  br i1 %exitcond.7, label %7, label %18
}

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) local_unnamed_addr #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) local_unnamed_addr #1

; Function Attrs: nounwind
declare void @free(i8* nocapture) local_unnamed_addr #1

attributes #0 = { nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="knl" "target-features"="+adx,+aes,+avx,+avx2,+avx512cd,+avx512er,+avx512f,+avx512pf,+bmi,+bmi2,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+lzcnt,+mmx,+movbe,+pclmul,+popcnt,+prefetchwt1,+rdrnd,+rdseed,+rtm,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsaveopt" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="knl" "target-features"="+adx,+aes,+avx,+avx2,+avx512cd,+avx512er,+avx512f,+avx512pf,+bmi,+bmi2,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+lzcnt,+mmx,+movbe,+pclmul,+popcnt,+prefetchwt1,+rdrnd,+rdseed,+rtm,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsaveopt" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 4.0.1-10 (tags/RELEASE_401/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
