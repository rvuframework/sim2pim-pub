; ModuleID = '../../benchmark/compiled/IR/tempFile.ll'
source_filename = "../../benchmark/tempFile.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

@B = common global [2097152 x i32] zeroinitializer, align 64
@C = common global [2097152 x i32] zeroinitializer, align 64
@A = common local_unnamed_addr global [2097152 x i32] zeroinitializer, align 64
@.str = private unnamed_addr constant [13 x i8] c"Result = %d\0A\00", align 1

; Function Attrs: nounwind uwtable
define i32 @vecsum() local_unnamed_addr #0 {
  br label %1

; <label>:1:                                      ; preds = %1, %0
  %indvars.iv32 = phi i64 [ 0, %0 ], [ %indvars.iv.next33, %1 ]
  %2 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @B, i64 0, i64 %indvars.iv32
  %3 = trunc i64 %indvars.iv32 to i32
  store i32 %3, i32* %2, align 4, !tbaa !1
  %4 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @C, i64 0, i64 %indvars.iv32
  %indvars.iv32.tr = trunc i64 %indvars.iv32 to i32
  %5 = shl i32 %indvars.iv32.tr, 1
  store i32 %5, i32* %4, align 4, !tbaa !1
  %indvars.iv.next33 = add nuw nsw i64 %indvars.iv32, 1
  %exitcond34 = icmp eq i64 %indvars.iv.next33, 2097152
  br i1 %exitcond34, label %.preheader.preheader, label %1, !llvm.loop !5

.preheader.preheader:                             ; preds = %1
  br label %.preheader

min.iters.checked:                                ; preds = %.preheader
  tail call void @llvm.x86.sse2.mfence()
  br label %vector.body

vector.body:                                      ; preds = %vector.body, %min.iters.checked
  %index = phi i64 [ 0, %min.iters.checked ], [ %index.next.1, %vector.body ]
  %6 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @B, i64 0, i64 %index
  %7 = bitcast i32* %6 to <2 x i32>*
  %wide.load = load <2 x i32>, <2 x i32>* %7, align 64, !tbaa !1
  %8 = getelementptr i32, i32* %6, i64 2
  %9 = bitcast i32* %8 to <2 x i32>*
  %wide.load38 = load <2 x i32>, <2 x i32>* %9, align 8, !tbaa !1
  %10 = getelementptr i32, i32* %6, i64 4
  %11 = bitcast i32* %10 to <2 x i32>*
  %wide.load39 = load <2 x i32>, <2 x i32>* %11, align 16, !tbaa !1
  %12 = getelementptr i32, i32* %6, i64 6
  %13 = bitcast i32* %12 to <2 x i32>*
  %wide.load40 = load <2 x i32>, <2 x i32>* %13, align 8, !tbaa !1
  %14 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @C, i64 0, i64 %index
  %15 = bitcast i32* %14 to <2 x i32>*
  %wide.load41 = load <2 x i32>, <2 x i32>* %15, align 64, !tbaa !1
  %16 = getelementptr i32, i32* %14, i64 2
  %17 = bitcast i32* %16 to <2 x i32>*
  %wide.load42 = load <2 x i32>, <2 x i32>* %17, align 8, !tbaa !1
  %18 = getelementptr i32, i32* %14, i64 4
  %19 = bitcast i32* %18 to <2 x i32>*
  %wide.load43 = load <2 x i32>, <2 x i32>* %19, align 16, !tbaa !1
  %20 = getelementptr i32, i32* %14, i64 6
  %21 = bitcast i32* %20 to <2 x i32>*
  %wide.load44 = load <2 x i32>, <2 x i32>* %21, align 8, !tbaa !1
  %22 = add nsw <2 x i32> %wide.load41, %wide.load
  %23 = add nsw <2 x i32> %wide.load42, %wide.load38
  %24 = add nsw <2 x i32> %wide.load43, %wide.load39
  %25 = add nsw <2 x i32> %wide.load44, %wide.load40
  %26 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @A, i64 0, i64 %index
  %27 = bitcast i32* %26 to <2 x i32>*
  store <2 x i32> %22, <2 x i32>* %27, align 64, !tbaa !1
  %28 = getelementptr i32, i32* %26, i64 2
  %29 = bitcast i32* %28 to <2 x i32>*
  store <2 x i32> %23, <2 x i32>* %29, align 8, !tbaa !1
  %30 = getelementptr i32, i32* %26, i64 4
  %31 = bitcast i32* %30 to <2 x i32>*
  store <2 x i32> %24, <2 x i32>* %31, align 16, !tbaa !1
  %32 = getelementptr i32, i32* %26, i64 6
  %33 = bitcast i32* %32 to <2 x i32>*
  store <2 x i32> %25, <2 x i32>* %33, align 8, !tbaa !1
  %index.next = or i64 %index, 8
  %34 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @B, i64 0, i64 %index.next
  %35 = bitcast i32* %34 to <2 x i32>*
  %wide.load.1 = load <2 x i32>, <2 x i32>* %35, align 32, !tbaa !1
  %36 = getelementptr i32, i32* %34, i64 2
  %37 = bitcast i32* %36 to <2 x i32>*
  %wide.load38.1 = load <2 x i32>, <2 x i32>* %37, align 8, !tbaa !1
  %38 = getelementptr i32, i32* %34, i64 4
  %39 = bitcast i32* %38 to <2 x i32>*
  %wide.load39.1 = load <2 x i32>, <2 x i32>* %39, align 16, !tbaa !1
  %40 = getelementptr i32, i32* %34, i64 6
  %41 = bitcast i32* %40 to <2 x i32>*
  %wide.load40.1 = load <2 x i32>, <2 x i32>* %41, align 8, !tbaa !1
  %42 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @C, i64 0, i64 %index.next
  %43 = bitcast i32* %42 to <2 x i32>*
  %wide.load41.1 = load <2 x i32>, <2 x i32>* %43, align 32, !tbaa !1
  %44 = getelementptr i32, i32* %42, i64 2
  %45 = bitcast i32* %44 to <2 x i32>*
  %wide.load42.1 = load <2 x i32>, <2 x i32>* %45, align 8, !tbaa !1
  %46 = getelementptr i32, i32* %42, i64 4
  %47 = bitcast i32* %46 to <2 x i32>*
  %wide.load43.1 = load <2 x i32>, <2 x i32>* %47, align 16, !tbaa !1
  %48 = getelementptr i32, i32* %42, i64 6
  %49 = bitcast i32* %48 to <2 x i32>*
  %wide.load44.1 = load <2 x i32>, <2 x i32>* %49, align 8, !tbaa !1
  %50 = add nsw <2 x i32> %wide.load41.1, %wide.load.1
  %51 = add nsw <2 x i32> %wide.load42.1, %wide.load38.1
  %52 = add nsw <2 x i32> %wide.load43.1, %wide.load39.1
  %53 = add nsw <2 x i32> %wide.load44.1, %wide.load40.1
  %54 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @A, i64 0, i64 %index.next
  %55 = bitcast i32* %54 to <2 x i32>*
  store <2 x i32> %50, <2 x i32>* %55, align 32, !tbaa !1
  %56 = getelementptr i32, i32* %54, i64 2
  %57 = bitcast i32* %56 to <2 x i32>*
  store <2 x i32> %51, <2 x i32>* %57, align 8, !tbaa !1
  %58 = getelementptr i32, i32* %54, i64 4
  %59 = bitcast i32* %58 to <2 x i32>*
  store <2 x i32> %52, <2 x i32>* %59, align 16, !tbaa !1
  %60 = getelementptr i32, i32* %54, i64 6
  %61 = bitcast i32* %60 to <2 x i32>*
  store <2 x i32> %53, <2 x i32>* %61, align 8, !tbaa !1
  %index.next.1 = add nsw i64 %index, 16
  %62 = icmp eq i64 %index.next.1, 2097152
  br i1 %62, label %middle.block, label %vector.body, !llvm.loop !9

.preheader:                                       ; preds = %.preheader.preheader, %.preheader
  %indvars.iv29 = phi i64 [ %indvars.iv.next30, %.preheader ], [ 0, %.preheader.preheader ]
  %63 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @B, i64 0, i64 %indvars.iv29
  %64 = bitcast i32* %63 to i8*
  tail call void @llvm.x86.sse2.clflush(i8* %64)
  %65 = getelementptr inbounds [2097152 x i32], [2097152 x i32]* @C, i64 0, i64 %indvars.iv29
  %66 = bitcast i32* %65 to i8*
  tail call void @llvm.x86.sse2.clflush(i8* %66)
  %indvars.iv.next30 = add nuw nsw i64 %indvars.iv29, 1
  %exitcond31 = icmp eq i64 %indvars.iv.next30, 2097152
  br i1 %exitcond31, label %min.iters.checked, label %.preheader, !llvm.loop !10

middle.block:                                     ; preds = %vector.body
  %67 = load i32, i32* getelementptr inbounds ([2097152 x i32], [2097152 x i32]* @A, i64 0, i64 0), align 64, !tbaa !1
  %68 = load i32, i32* getelementptr inbounds ([2097152 x i32], [2097152 x i32]* @A, i64 0, i64 1048576), align 64, !tbaa !1
  %69 = add nsw i32 %68, %67
  %70 = load i32, i32* getelementptr inbounds ([2097152 x i32], [2097152 x i32]* @A, i64 0, i64 2097151), align 4, !tbaa !1
  %71 = add nsw i32 %69, %70
  %72 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([13 x i8], [13 x i8]* @.str, i64 0, i64 0), i32 %71)
  %73 = load i32, i32* getelementptr inbounds ([2097152 x i32], [2097152 x i32]* @A, i64 0, i64 0), align 64, !tbaa !1
  %74 = load i32, i32* getelementptr inbounds ([2097152 x i32], [2097152 x i32]* @A, i64 0, i64 1048576), align 64, !tbaa !1
  %75 = add nsw i32 %74, %73
  %76 = load i32, i32* getelementptr inbounds ([2097152 x i32], [2097152 x i32]* @A, i64 0, i64 2097151), align 4, !tbaa !1
  %77 = add nsw i32 %75, %76
  ret i32 %77
}

; Function Attrs: nounwind
declare void @llvm.x86.sse2.clflush(i8*) #1

; Function Attrs: nounwind
declare void @llvm.x86.sse2.mfence() #1

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) local_unnamed_addr #2

attributes #0 = { nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="knl" "target-features"="+adx,+aes,+avx,+avx2,+avx512cd,+avx512er,+avx512f,+avx512pf,+bmi,+bmi2,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+lzcnt,+mmx,+movbe,+pclmul,+popcnt,+prefetchwt1,+rdrnd,+rdseed,+rtm,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsaveopt" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind }
attributes #2 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="knl" "target-features"="+adx,+aes,+avx,+avx2,+avx512cd,+avx512er,+avx512f,+avx512pf,+bmi,+bmi2,+cx16,+f16c,+fma,+fsgsbase,+fxsr,+lzcnt,+mmx,+movbe,+pclmul,+popcnt,+prefetchwt1,+rdrnd,+rdseed,+rtm,+sse,+sse2,+sse3,+sse4.1,+sse4.2,+ssse3,+x87,+xsave,+xsaveopt" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 4.0.1-10 (tags/RELEASE_401/final)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"int", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = distinct !{!5, !6, !7, !8}
!6 = !{!"llvm.loop.vectorize.width", i32 1}
!7 = !{!"llvm.loop.interleave.count", i32 1}
!8 = !{!"llvm.loop.unroll.disable"}
!9 = distinct !{!9, !6, !7}
!10 = distinct !{!10, !6, !7, !8}
