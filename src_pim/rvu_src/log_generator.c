


#include "log_generator.h"


void print_log(const char* LABEL, const uint8_t IM_PRINTS_LvL, const uint8_t EXEC_PRINTS_LvL) {
    
    if (IM_PRINTS_LvL > 0) {
        printf("++++++++++++++++++++++++++ METRICS FOR %s MODULE ++++++++++++++++\n",LABEL);
        printf("IM_CURRENT_CY = @%ld\tIM_PREVIOUS_CY = %ld\n", IM_CURRENT_CY, IM_PREVIOUS_CY);
        printf("--------------------------------------------------------------------------------\n");
        //fflush(LABEL);
    }
    else {
        printf("PRINTING NOTHING\n");
    }

}