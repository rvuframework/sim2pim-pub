

#include "dispatcher.h"
#include "rvu.h"

//this function only emits instructions to the PIM, and call PIM to be simulated
void doWork(uint64_t ins0, uint64_t ins1, uint64_t ls, uint64_t cycle) {
   
    printf("\n\n\nHOST TO PIM: DISPATCHER\n");
    printf("HOST TO PIM: Instruction @%ld = 0x%016lx 0x%016lx at address 0x%016lx\n", cycle, ins0, ins1, ls);
    
    /**call PIM here**/
    main_rvu(ins0, ins1, ls, cycle); 
    //main_memristor(ins0, ins1, ls, cycle);
}