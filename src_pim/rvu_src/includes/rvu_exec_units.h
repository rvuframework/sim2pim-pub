#ifndef RVU_EXEC_UNITS_H
#define RVU_EXEC_UNITS_H

#include <inttypes.h>
#include <stdio.h>

#include "rvu.h"
#include "log_generator.h"


void rvu_decode_stage(void);
void rvu_exec_stage(void);
void rvu_sent_release(void);
void rvu_sent_response(void);
 


void rvu_exec_units(void);



#endif