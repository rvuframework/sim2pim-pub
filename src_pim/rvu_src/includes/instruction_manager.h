#ifndef INSTRUCTION_MANAGER_H
#define INSTRUCTION_MANAGER_H

#include <inttypes.h>
#include <stdio.h>

#include "rvu.h"
#include "log_generator.h"

void instruction_manager(uint64_t ins0, uint64_t ins1, uint64_t ls, uint64_t cycle);


#endif