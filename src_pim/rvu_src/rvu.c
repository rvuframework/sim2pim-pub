
//PIM RVU SIMPLE SIMULATION




#include "rvu.h"


void main_rvu(uint64_t ins0, uint64_t ins1, uint64_t ls, uint64_t cycle) {
    printf("\n\n\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
    printf("RVU: entering PIM\n");
    printf("RVU: Instruction 0x%016lx 0x%016lx at address 0x%016lx\nCycle = @%ld\n" , ins0, ins1, ls, cycle);    
    
    //INSTRUCTION MANAGER
        //PIM_CHECKER 
        //PRE_DECODER
        //NEXT_ADDRESS_CHECKER if PIM LOAD/STORE instruction
            //INSTRUCTION_COMPOSER to compose LS instruction
        //INSTRUCTION_SPLITTER if PIM instructions bigger than 256B
        //INTERVAULT_MANAGER if PIM instruction uses more than one RVU unit
        //INSTRUCTION_EMITTER 
        
    //RVU UNIT
        //PRE_DECODER
            //IF LS THEN GOES TO THE COMMON LS MEMORY REQUEST BUFFER AND RESERVES A SLOT
            //IN PARALLEL GOES TO THE RVU INSTRUCTION BUFFER
                //IF LOAD THEN RVU WAITS MEMORY RESPONSE
                //IF STORE THEN MEMORY WAITS RVU RESPONSE
            //IF ACQUIRE THEN WAITS FOR RELEASE
                //SEND RESPONSE
            //IF NOT LS AND NOT ACQUIRE THEN GOES TO THE RVU INSTRUCTION BUFFER
                //RVU OPERATES
                    //IF SOURCE IS FROM ANOTHER RVU THEN EMITTES RELEASE TO THE RVU SOURCE
                        //WAITS RESPONSE
                //RVU COMPLETES THE OPERATION
                    
               
                    
        
    
    
    //CALLING INSTRUCTION MANAGER
    instruction_manager(ins0, ins1, ls, cycle);
    rvu_exec_units();
    
    
    //print_log();
    
}

