

#include "instruction_manager.h"

    //INSTRUCTION MANAGER
        //PIM_CHECKER 
        //PRE_DECODER
        //NEXT_ADDRESS_CHECKER if PIM LOAD/STORE instruction
            //INSTRUCTION_COMPOSER to compose LS instruction
        //INSTRUCTION_SPLITTER if PIM instruction bigger than 256B
        //INTERVAULT_MANAGER if PIM instruction uses more than one RVU unit
        //INSTRUCTION_EMITTER 


void instruction_manager(uint64_t ins0, uint64_t ins1, uint64_t ls, uint64_t cycle) {
    #if (PRINTS_IM_ENTER == true)
        printf("\n\n\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        printf("INSTRUCTION_MANAGER: Entering Instruction_Manager\n");
        printf("INSTRUCTION_MANAGER: Instruction = 0x%016lx 0x%016lx at address 0x%016lx \nCycle @%ld \n", ins0, ins1, ls, cycle);
        printf("--------------------------------------------------------------------------------\n");
    #endif
    

    uint32_t current_opcode;
    uint32_t current_opsize;
    uint32_t current_datatype;
    uint32_t current_rvu_destination;
    uint32_t current_reg_destination;
    uint32_t current_rvu_source1;
    uint32_t current_reg_source1;
    uint32_t current_rvu_source2;
    uint32_t current_reg_source2;
    uint64_t current_memory_address;
    uint8_t  isLS, isIntervault, isAcquire, isRelease;
    
    uint64_t cycles_diff = 0;
    // uint64_t IM_CURRENT_CY = 0;

    #if (METRICS_ON == true)
        if(IM_PREVIOUS_CY != 0) {
            cycles_diff = cycle - IM_PREVIOUS_CY;
        }
        IM_PREVIOUS_CY = cycle;        
    #endif
    #if (METRIC_PRINTS == true)
        printf("\n++++++++++++++++++++++++++++++++++++ METRICS +++++++++++++++++++++++++++++++++++\n");
        printf("IM_CURRENT_CY = @%ld\tIM_PREVIOUS_CY = %ld\tcycles_diff = %ld\n", IM_CURRENT_CY, IM_PREVIOUS_CY, cycles_diff);
        printf("--------------------------------------------------------------------------------\n");
    #endif
    
    //---------------PIM_CHECKER-------------------//
    /// PIM_CHECKER checks whether the address being accessed is PIM or NOT
    /// JUST TO CONSIDER THE TIMINGS
    #if (METRICS_ON == true)
        IM_CURRENT_CY += IM_PIM_CHECKER_CY;
    #endif
    #if (PRINTS_IM == true)
        printf("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        printf("INSTRUCTION_MANAGER: PIM_CHECKER: Entering PIM_CHECKER\n");
        printf("INSTRUCTION_MANAGER: PIM_CHECKER: Checking if PIM Address-mapped being accessed\n");
        printf("--------------------------------------------------------------------------------\n");
    // #if (PRINTS == true)
        // print_log("IM PIM_CHECKER", 1, 0);
    // #endif        
    #endif
    // #if (METRIC_PRINTS == true)
        // printf("\n++++++++++++++++++++++++++++++++++++ METRICS +++++++++++++++++++++++++++++++++++\n");
        // printf("IM_CURRENT_CY = @%ld\tIM_PREVIOUS_CY = %ld\tcycles_diff = %ld\n", IM_CURRENT_CY, IM_PREVIOUS_CY, cycles_diff);
        // printf("--------------------------------------------------------------------------------\n");
    // #endif    
    //--------------END---------------//
    
    
    //---------------PRE_DECODER-------------------//
    /// IF PIM_CHECKER returns TRUE, then PRE_DECODER decodes the PIM INSTRUCTION
    current_opcode = (ins0 >> (64-OPCODE)) & 0x00FF;     //get the OPCODE
    current_opsize = (ins0 >> (64-OPCODE-OP_SIZE)) & 0x001F; //get the OPSIZE
    current_datatype = (ins0 >> (64-OPCODE-OP_SIZE-DATA_TYPE)) & 0x0007; //get the DATA_TYPE
    current_rvu_destination = (ins0 >> (64-OPCODE-OP_SIZE-DATA_TYPE-RVU_ADDRESS)) & 0x001F; //get the RVU DEST
    current_reg_destination = (ins0 >> (64-OPCODE-OP_SIZE-DATA_TYPE-RVU_ADDRESS-RVU_REG_ADDRESS)) & 0x3FFF; //get the REG DEST
    current_rvu_source1 = (ins0 >> (64-OPCODE-OP_SIZE-DATA_TYPE-RVU_ADDRESS-RVU_REG_ADDRESS-RVU_ADDRESS)) & 0x001F; //get the RVU_SOURCE1
    current_reg_source1 = (ins0 >> (64-OPCODE-OP_SIZE-DATA_TYPE-RVU_ADDRESS-RVU_REG_ADDRESS-RVU_ADDRESS-RVU_REG_ADDRESS)) & 0x3FFF;
    current_rvu_source2 = (ins0 >> (64-OPCODE-OP_SIZE-DATA_TYPE-RVU_ADDRESS-RVU_REG_ADDRESS-RVU_ADDRESS-RVU_REG_ADDRESS-RVU_ADDRESS)) & 0x001F;
    current_reg_source2 = ((ins0 & 0x001F) | 0x1FF) & ((ins1 >> (64-9)) & 0x01FF);
    
    #if (METRICS_ON == true)
        IM_CURRENT_CY += IM_PRE_DECODER_CY;
    #endif    
            
    #if (PRINTS == true)
        print_log("INSTRUCTION_MANAGER: PRE_DECODER", 1, 0); 
    #endif
    #if (METRIC_PRINTS == true)
        // printf("\n++++++++++++++++++++++++++++++++++++ METRICS +++++++++++++++++++++++++++++++++++\n");
        // printf("IM_CURRENT_CY = @%ld\tIM_PREVIOUS_CY = %ld\tcycles_diff = %ld\n", IM_CURRENT_CY, IM_PREVIOUS_CY, cycles_diff);
        // printf("--------------------------------------------------------------------------------\n");
    #endif
    
    //-------------SET FLAGS AFTER PRE_DECODER---------------//
    //check whether the instruction accesses memory
    if ((current_opcode == LOAD) || (current_opcode == STORE)) { //TODO: we need to check for Broadcast !!
        isLS = 1;
    } else {
        isLS = 0;
    }
    
    //check whether the instruction accesses multiple PIM units
    if ((isLS == false) && (current_rvu_destination != current_rvu_source1) && (current_rvu_source1 != current_rvu_source2)){
        isIntervault = 2;        
    } else if ((isLS == false) && (current_rvu_destination != current_rvu_source1) && (current_rvu_source1 == current_rvu_source2)){
        isIntervault = 1;
    } else {
        isIntervault = 0;
    }
    #if (PRINTS_IM == true)
        printf("\n+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        printf("INSTRUCTION_MANAGER: SETTING FLAGS\n");
        printf("--------------------------------------------------------------------------------\n");
    #endif    
    #if (METRIC_PRINTS == true)
        printf("\n++++++++++++++++++++++++++++++++++++ METRICS +++++++++++++++++++++++++++++++++++\n");
        printf("IM_CURRENT_CY = @%ld\tIM_PREVIOUS_CY = %ld\tcycles_diff = %ld\n", IM_CURRENT_CY, IM_PREVIOUS_CY, cycles_diff);
        printf("--------------------------------------------------------------------------------\n");
    #endif    
    //--------------END---------------//    
    
    
    //-------------NEXT_ADDRESS_CHECKER and INSTRUCTION_COMPOSER---------------//
    if(isLS == 1) { 
        #if (PRINTS_IM == true)
            printf("\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
            printf("INSTRUCTION_MANAGER: NEXT_ADDRESS_CHECKER: GETTING Load/Store address from the next instruction\n");    
            printf("INSTRUCTION_MANAGER: INSTRUCTION_COMPOSER: Composing Load/Store Instruction\n");
        #endif
        ///if LS operation, then the next memory access is captured
        current_memory_address = ls; //TODO: we are emitting this "data" all together, but they should be in different host's emissions. We need to wait for next instruction, and check the data being stored that must be equal to the previous PIM instruction
        #if (METRICS_ON == true)
            IM_CURRENT_CY += IM_INSTRUCTION_COMPOSER_CY;
        #endif
        #if (PRINTS_IM == true)
            printf("INSTRUCTION_MANAGER: INSTRUCTION_COMPOSER: current_memory_address = 0x%016lx\n", current_memory_address);
            printf("--------------------------------------------------------------------------------\n");
        #endif
        #if (METRIC_PRINTS == true)
            printf("\n++++++++++++++++++++++++++++++++++++ METRICS +++++++++++++++++++++++++++++++++++\n");
            printf("IM_CURRENT_CY = @%ld\tIM_PREVIOUS_CY = %ld\tcycles_diff = %ld\n", IM_CURRENT_CY, IM_PREVIOUS_CY, cycles_diff);
            printf("--------------------------------------------------------------------------------\n");
        #endif                  
    } else {
        current_memory_address = 0xFFFFFFFFFFFFFFFF; //dummy value, this address will not be used anyway.
    }
  
    //------------END----------------//



    //ZEROING OUTPUT BUFFERS
    for(uint32_t i = 0; i < NUMBER_OF_RVU_UNITS; i++) {
        IM_OUT_BUFFER[i].valid = false;
    }
    
    //-------------INSTRUCTION_SPLITTER---------------//
    if(current_opsize > B256) {
        #if (METRICS_ON == true)
            IM_CURRENT_CY += IM_INSTRUCTION_SPLITTER_CY;
        #endif
        #if (PRINTS_IM == true)
            printf("\n\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
            printf("INSTRUCTION_MANAGER: INSTRUCTION_SPLITTER: SPLITTING CODE\n");
            printf("--------------------------------------------------------------------------------\n"); 
        #endif    
        #if (METRIC_PRINTS == true)
            printf("\n++++++++++++++++++++++++++++++++++++ METRICS +++++++++++++++++++++++++++++++++++\n");
            printf("IM_CURRENT_CY = @%ld\tIM_PREVIOUS_CY = %ld\tcycles_diff = %ld\n", IM_CURRENT_CY, IM_PREVIOUS_CY, cycles_diff);
            printf("--------------------------------------------------------------------------------\n");
        #endif            
    }
    
    //OFFLOADING INSTRUCTIONS TO THE DESTINATION RVU UNIT, which is responsible for managing requests 
    if(current_opsize > B256) {
        for(uint32_t i = current_rvu_destination; i < (current_rvu_destination + ((2 << (current_opsize+1))/256)); i++) {
            IM_OUT_BUFFER[i].valid = true;
            IM_OUT_BUFFER[i].isLS = isLS;
            IM_OUT_BUFFER[i].isINTERVAULT = isIntervault;
            IM_OUT_BUFFER[i].isACQUIRE = 0;
            IM_OUT_BUFFER[i].isRELEASE = isIntervault;
            IM_OUT_BUFFER[i].OPERATION = current_opcode;
            IM_OUT_BUFFER[i].OPER_SIZE = B256;
            IM_OUT_BUFFER[i].OPER_TYPE = current_datatype;
            IM_OUT_BUFFER[i].RVU_DST   = i; //current_rvu_destination+(i-current_rvu_destination);
            IM_OUT_BUFFER[i].REG_DST   = current_reg_destination;
            IM_OUT_BUFFER[i].RVU_SRC1  = current_rvu_source1+(i-current_rvu_destination);
            IM_OUT_BUFFER[i].REG_SRC1  = current_reg_source1;
            IM_OUT_BUFFER[i].RVU_SRC2  = current_rvu_source2+(i-current_rvu_destination);
            IM_OUT_BUFFER[i].REG_SRC2  = current_reg_source2;
            IM_OUT_BUFFER[i].LS_ADDR   = current_memory_address+((i-current_rvu_destination)*256); //TODO: i*(256/current_datatype)
        }            
    } else {
        IM_OUT_BUFFER[current_rvu_destination].valid = true;
        IM_OUT_BUFFER[current_rvu_destination].isLS = isLS;
        IM_OUT_BUFFER[current_rvu_destination].isINTERVAULT = isIntervault;
        IM_OUT_BUFFER[current_rvu_destination].isACQUIRE = 0;
        IM_OUT_BUFFER[current_rvu_destination].isRELEASE = isIntervault;
        IM_OUT_BUFFER[current_rvu_destination].OPERATION = current_opcode;
        IM_OUT_BUFFER[current_rvu_destination].OPER_SIZE = current_opsize;
        IM_OUT_BUFFER[current_rvu_destination].OPER_TYPE = current_datatype;
        IM_OUT_BUFFER[current_rvu_destination].RVU_DST   = current_rvu_destination;
        IM_OUT_BUFFER[current_rvu_destination].REG_DST   = current_reg_destination;
        IM_OUT_BUFFER[current_rvu_destination].RVU_SRC1  = current_rvu_source1;
        IM_OUT_BUFFER[current_rvu_destination].REG_SRC1  = current_reg_source1;
        IM_OUT_BUFFER[current_rvu_destination].RVU_SRC2  = current_rvu_source2;
        IM_OUT_BUFFER[current_rvu_destination].REG_SRC2  = current_reg_source2;
        IM_OUT_BUFFER[current_rvu_destination].LS_ADDR   = current_memory_address;
    }
    //------------END----------------//    
    
    //-------------SETTING FOR INTERVAULT---------------//
    if(isIntervault != false) {
        if(current_rvu_destination != current_rvu_source1) {
            #if (PRINTS_IM == true)
                printf("INSTRUCTION_MANAGER: SETTING INTERVAULT at SOURCE1\n");
            #endif
            if(current_opsize > B256) {
                for(uint32_t i = current_rvu_source1; i < (current_rvu_source1 + ((2 << (current_opsize+1))/256)); i++) {
                    IM_OUT_BUFFER[i].valid = true;
                    IM_OUT_BUFFER[i].isLS = isLS;
                    IM_OUT_BUFFER[i].isINTERVAULT = isIntervault;
                    IM_OUT_BUFFER[i].isACQUIRE = isIntervault;
                    IM_OUT_BUFFER[i].isRELEASE = 0;
                    IM_OUT_BUFFER[i].OPERATION = current_opcode;
                    IM_OUT_BUFFER[i].OPER_SIZE = current_opsize;
                    IM_OUT_BUFFER[i].OPER_TYPE = current_datatype;
                    IM_OUT_BUFFER[i].RVU_DST   = current_rvu_destination;
                    IM_OUT_BUFFER[i].REG_DST   = current_reg_destination;
                    IM_OUT_BUFFER[i].RVU_SRC1  = current_rvu_source1;
                    IM_OUT_BUFFER[i].REG_SRC1  = current_reg_source1;
                    IM_OUT_BUFFER[i].RVU_SRC2  = current_rvu_source2;
                    IM_OUT_BUFFER[i].REG_SRC2  = current_reg_source2;
                    IM_OUT_BUFFER[i].LS_ADDR   = current_memory_address;
                }                
            } else { 
                IM_OUT_BUFFER[current_rvu_source1].valid = true;
                IM_OUT_BUFFER[current_rvu_source1].isLS = isLS;
                IM_OUT_BUFFER[current_rvu_source1].isINTERVAULT = isIntervault;
                IM_OUT_BUFFER[current_rvu_source1].isACQUIRE = isIntervault;
                IM_OUT_BUFFER[current_rvu_source1].isRELEASE = 0;
                IM_OUT_BUFFER[current_rvu_source1].OPERATION = current_opcode;
                IM_OUT_BUFFER[current_rvu_source1].OPER_SIZE = current_opsize;
                IM_OUT_BUFFER[current_rvu_source1].OPER_TYPE = current_datatype;
                IM_OUT_BUFFER[current_rvu_source1].RVU_DST   = current_rvu_destination;
                IM_OUT_BUFFER[current_rvu_source1].REG_DST   = current_reg_destination;
                IM_OUT_BUFFER[current_rvu_source1].RVU_SRC1  = current_rvu_source1;
                IM_OUT_BUFFER[current_rvu_source1].REG_SRC1  = current_reg_source1;
                IM_OUT_BUFFER[current_rvu_source1].RVU_SRC2  = current_rvu_source2;
                IM_OUT_BUFFER[current_rvu_source1].REG_SRC2  = current_reg_source2;
                IM_OUT_BUFFER[current_rvu_source1].LS_ADDR   = current_memory_address;
            }
        }
            
        if((current_rvu_destination != current_rvu_source2) && (current_rvu_source1 != current_rvu_source2)) {
            #if (PRINTS_IM == true)
                printf("INSTRUCTION_MANAGER: SETTING INTERVAULT at SOURCE2\n");
            #endif
            if(current_opsize > B256) {
                for(uint32_t i = current_rvu_source2; i < (current_rvu_source2 + ((2 << (current_opsize+1))/256)); i++) {
                    IM_OUT_BUFFER[i].valid = true;
                    IM_OUT_BUFFER[i].isLS = isLS;
                    IM_OUT_BUFFER[i].isINTERVAULT = isIntervault;
                    IM_OUT_BUFFER[i].isACQUIRE = isIntervault;
                    IM_OUT_BUFFER[i].isRELEASE = 0;
                    IM_OUT_BUFFER[i].OPERATION = current_opcode;
                    IM_OUT_BUFFER[i].OPER_SIZE = current_opsize;
                    IM_OUT_BUFFER[i].OPER_TYPE = current_datatype;
                    IM_OUT_BUFFER[i].RVU_DST   = current_rvu_destination;
                    IM_OUT_BUFFER[i].REG_DST   = current_reg_destination;
                    IM_OUT_BUFFER[i].RVU_SRC1  = current_rvu_source1;
                    IM_OUT_BUFFER[i].REG_SRC1  = current_reg_source1;
                    IM_OUT_BUFFER[i].RVU_SRC2  = current_rvu_source2;
                    IM_OUT_BUFFER[i].REG_SRC2  = current_reg_source2;
                    IM_OUT_BUFFER[i].LS_ADDR   = current_memory_address;
                }                
            } else { 
                IM_OUT_BUFFER[current_rvu_source2].valid = true;
                IM_OUT_BUFFER[current_rvu_source2].isLS = isLS;
                IM_OUT_BUFFER[current_rvu_source2].isINTERVAULT = isIntervault;
                IM_OUT_BUFFER[current_rvu_source2].isACQUIRE = isIntervault;
                IM_OUT_BUFFER[current_rvu_source2].isRELEASE = 0;
                IM_OUT_BUFFER[current_rvu_source2].OPERATION = current_opcode;
                IM_OUT_BUFFER[current_rvu_source2].OPER_SIZE = current_opsize;
                IM_OUT_BUFFER[current_rvu_source2].OPER_TYPE = current_datatype;
                IM_OUT_BUFFER[current_rvu_source2].RVU_DST   = current_rvu_destination;
                IM_OUT_BUFFER[current_rvu_source2].REG_DST   = current_reg_destination;
                IM_OUT_BUFFER[current_rvu_source2].RVU_SRC1  = current_rvu_source1;
                IM_OUT_BUFFER[current_rvu_source2].REG_SRC1  = current_reg_source1;
                IM_OUT_BUFFER[current_rvu_source2].RVU_SRC2  = current_rvu_source2;
                IM_OUT_BUFFER[current_rvu_source2].REG_SRC2  = current_reg_source2;
                IM_OUT_BUFFER[current_rvu_source2].LS_ADDR   = current_memory_address;
            }
        }            
    }
    
    //-------------INSTRUCTION_EMITTER---------------//
    #if (METRICS_ON == true)
        IM_CURRENT_CY += IM_INSTRUCTION_EMITTER_CY;
    #endif
    #if (PRINTS_IM == true)
        printf("\n\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n");
        printf("INSTRUCTION_MANAGER: INSTRUCTION_EMITTER: EMITTING CODE\n");
        printf("--------------------------------------------------------------------------------\n\n");
    #endif

    //printf("IM_OUT_BUFFER: OPERATION = 0x%08x\n",IM_OUT_BUFFER.OPERATION);
    //------------END----------------//
    
    
    #if (METRICS_ON == true)
        if(cycles_diff < IM_CURRENT_CY) {
            IM_CURRENT_CY -= cycles_diff;
        }
        IM_TOTAL_CY += IM_CURRENT_CY;
    #endif
    
    #if (METRIC_PRINTS == true)
        printf("\n++++++++++++++++++++++++++++++++++++ METRICS +++++++++++++++++++++++++++++++++++\n");
        printf("IM_CURRENT_CYCLES = %ld \n", IM_CURRENT_CY);
        printf("IM_TOTAL_CYCLES = %ld \n\n", IM_TOTAL_CY);
        printf("--------------------------------------------------------------------------------\n");
    #endif
}
    
    
    
    