#!/usr/bin/python


########### PARSER NAIVE MODE --- NO MAGIC TRICKS ###########

#############################################################
#############                                   #############
############# ASM PIM to ASM X86 + EMBEDDED PIM #############
#############                                   #############
#############################################################

########## THIS VERSION ONLY WORKS WITH AT&T SYNTAX #########
## A.K.A operation will occur from left to right
## LOAD MEM_SRC, DST
## STORE SRC, MEM_DST
## ADD SRC1, SRC2, DST

import sys
import os
import re
from bitstring import BitArray ##https://www.bogotobogo.com/python/python_bits_bytes_bitstring_constBitStream.php

input_asm = sys.argv[1]
output_asm = sys.argv[2]

input_file  = open(input_asm, "r")
output_file = open(output_asm, "w")


PIM_NAME = 'PIM_ADDR'
PIM_ADDR = 'PIM_LS'

this_rvu = 0

####"GLOBAL" variables
SRC_to_SAVE=[]
DST_to_SAVE=[]

#################################################################
def INSTRUCTION_ENCODING(field):
    """ INSTRUCTIONS OPCODE """
    if field=='LOAD':
        return 0
    elif field=='STORE':
        return 1
    elif field=='VADD':
        return 2
    elif field=='VXOR':
        return 6
    elif field=='VMUL':
        return 8
    elif field=='VMOVV':
        return 10
    elif field=='VMOVXMMtoPIM':
        return 11
    elif field=='VPERM':
        return 12
    elif field=='BROADCASTS':
        return 13
    else:
        return 'INSTRUCTION NOT SUPPORTED'
#################################################################
def OPERATION_SIZE_ENCODING(field):
    if field=='16B':
        # print "SIZE 64B ENCODED"
        return 2
    elif field=='32B':
        # print "SIZE 64B ENCODED"
        return 3
    elif field=='64B':
        # print "SIZE 64B ENCODED"
        return 4
    elif field=='128B':
        # print "SIZE 128B ENCODED"
        return 5
    elif field=='256B':
        # print "SIZE 256B ENCODED"
        return 6
    elif field=='512B':
        # print "SIZE 512B ENCODED"
        return 7
    elif field=='1024B':
        # print "SIZE 1024B ENCODED"
        return 8
    elif field=='2048B':
        # print "SIZE 2048B ENCODED"
        return 9
    elif field=='4096B':
        # print "SIZE 4096B ENCODED"
        return 10
    elif field=='8192B':
        # print "SIZE 8192B ENCODED"
        return 11
    else:
        return 'OP SIZE NOT SUPPORTED'
#################################################################
def DATA_TYPE_ENCODING(field):
    if field=='BWORD':
        # print "DATA TYPE BYTE ENCODED"
        return 0
    elif field=='HWORD' or field=='WORD':
        # print "DATA TYPE HALF ENCODED"
        return 1
    elif field=='DWORD':
        # print "DATA TYPE DWORD ENCODED"
        return 2
    elif field=='QWORD':
        # print "DATA TYPE QWORD ENCODED"
        return 3
    else:
        return 'DATA TYPE NOT SUPPORTED'
#################################################################

#################################################################
for line in input_file:
    #\/\/\/\/\/\/\/\/\/ NECESSARY FOR MT \/\/\/\/\/\/\/\/\/#
    if re.match("#rvu per cores for MT =(.*)",line):
        [int(s) for s in line.split() if s.isdigit()]
        RVU_per_Core = s

    if re.match("(.*)PIM(.*)",line):
        if re.match("(.*)first_RVU: (.*)",line):
            [int(s) for s in line.split() if s.isdigit()]
            this_rvu = s
        RVU_number = int(this_rvu)/int(RVU_per_Core)

        PIM_NAME = 'PIM_ADDR+'+str(RVU_number*16)
        PIM_ADDR = 'PIM_LS+'+str(RVU_number*8)

        #/\/\/\/\/\/\/\/\/\ NECESSARY FOR MT /\/\/\/\/\/\/\/\/\#

        #print line
        CLEAN_LINE = line.split("#", 1)[0] #clean line
        # print CLEAN_LINE
        CLEAN_LINE = CLEAN_LINE.replace('\t',' ')
        PIM_INSTRUCTION = CLEAN_LINE.split(' ')
        # print PIM_INSTRUCTION
        # PIM_INSTRUCTION = re.findall("PIM\w+",CLEAN_LINE) #get only the OPCODE MNMONIC
        # print PIM_INSTRUCTION
        PIM_OPCODE_FIELDS = PIM_INSTRUCTION[1].split('_')
        # print PIM_OPCODE_FIELDS
        PIM_OPSIZE = PIM_OPCODE_FIELDS[1]
        # print "PIM_OPSIZE = %s" %PIM_OPSIZE
        PIM_OPERATION = PIM_OPCODE_FIELDS[2]
        # print "PIM_OPERATION = %s" %PIM_OPERATION
        if PIM_OPERATION=='VMOVV' or PIM_OPERATION=='VMOVXMMtoPIM': ##MOVE Instructions don't care about data type
            PIM_DATA_TYPE = 'DWORD' #a generic one
        else:
            PIM_DATA_TYPE = PIM_OPCODE_FIELDS[3]
        # print "PIM_DATA_TYPE = %s" %PIM_DATA_TYPE
        ##############################################
        if PIM_OPERATION=='LOAD' or PIM_OPERATION=='BROADCASTS':
            # print "GENERATING %s\n" %PIM_OPERATION
            SRC = PIM_INSTRUCTION[2].replace('),',')')
            # print "SRC = %s" %SRC
            ## SRC_to_SAVE=[]
            #
            for i in range(0, len(SRC)):
                if SRC[i] == '%':
                    SRC_to_SAVE.append(SRC[i:4+i])
            #
            # print "SRC_to_SAVE = %s" %SRC_to_SAVE
            DST = PIM_INSTRUCTION[3]
            # print "DESTINATION = %s" %DST
            RVU_DST = DST.split('_')
            # print RVU_DST
            RVU_DST = RVU_DST[1]
            # print "RVU_DST = %s" %RVU_DST
            RVU_REG_DST = DST.split('_')
            # print REG_DST
            RVU_REG_DST = RVU_REG_DST[-1]
            # print "RVU_REG_DST = %s" %RVU_REG_DST
            ####################################
            ###generate the instruction
            base_instruction128bits = BitArray(uint=INSTRUCTION_ENCODING(PIM_OPERATION), length=8) + BitArray(uint=OPERATION_SIZE_ENCODING(PIM_OPSIZE), length=5) + BitArray(uint=DATA_TYPE_ENCODING(PIM_DATA_TYPE), length=3) + BitArray(uint=int(RVU_DST), length=5) + BitArray(uint=int(RVU_REG_DST), length=14)+BitArray(uint=0, length=(128-(8+5+3+5+14)))
            ####################################
            output_file.write("    ###%s" % line)
            #
            if PIM_OPERATION=='BROADCASTS':
                for i in range (0, (2**(OPERATION_SIZE_ENCODING(PIM_OPSIZE))/16)):
                    output_file.write("    clflush\t%s \t\t\t#FLUSH TO KEEP CACHE COHERENCE\n" %SRC)
                output_file.write("    sfence\n")
            #
            output_file.write("    pushq %rax\n")
            output_file.write("    mfence\n")
            output_file.write("    movq\t$0x%s " % base_instruction128bits.hex[0:16])
            output_file.write(", %rax\n")
            output_file.write("    movq\t%rax, %xmm0\n")
            output_file.write("    movq\t$0x%s " % base_instruction128bits.hex[16:32])
            output_file.write(", %rax\n")
            output_file.write("    movq\t%rax, %xmm1\n")
            output_file.write("    punpcklqdq\t%xmm1,%xmm0\n")
            output_file.write("    movntdq\t%xmm0,"+PIM_NAME+"(%rip)\n")
            output_file.write("    mfence\n")
            output_file.write("    #######################\n")
            if "%rax" in SRC_to_SAVE and "%r10" not in SRC_to_SAVE:
                output_file.write("    popq %rax\n")    
                output_file.write("    pushq %r10\n")
                output_file.write("    movq %rax, %r10\n")
            output_file.write("    leaq %s," %SRC)
            output_file.write("%rax\t\t #CLFLUSH ADDRESS\n")
            output_file.write("    movq\t%rax,"+PIM_ADDR+"(%rip)\n")
            output_file.write("    #######################\n")
            if "%rax" in SRC_to_SAVE and "%r10" not in SRC_to_SAVE:
                output_file.write("    movq %r10, %rax\n")                
                output_file.write("    popq %r10\n")
                output_file.write("    pushq %rax\n")                             
            output_file.write("    callq SimulatorCall\n")
            output_file.write("    #######################\n")
            output_file.write("    popq %rax\n")
            output_file.write("###### SIMULATION CALL ENDS ######\n")
            output_file.write("### END PIM LOAD ###\n")
        ##############################################
        elif PIM_OPERATION=='STORE':
            # print "GENERATING %s\n" %PIM_OPERATION
            SRC = PIM_INSTRUCTION[2].replace(',','')
            # print "SRC = %s" %SRC
            RVU_SRC = SRC.split('_')
            # print RVU_SRC
            RVU_SRC = RVU_SRC[1]
            # print "RVU_SRC = %s" %RVU_SRC
            RVU_REG_SRC = SRC.split('_')
            # print REG_SRC
            RVU_REG_SRC = RVU_REG_SRC[-1]
            # print "RVU_REG_SRC = %s" %RVU_REG_SRC
            DST = PIM_INSTRUCTION[3]
            # print "DST = %s" %DST
            ## DST_to_SAVE=[]
            #
            for i in range(0, len(DST)):
                if DST[i] == '%':
                    DST_to_SAVE.append(DST[i:4+i])
            #
            # print "DST_to_SAVE = %s" %DST_to_SAVE
            ####################################
            ###generate the instruction
            base_instruction128bits = BitArray(uint=INSTRUCTION_ENCODING(PIM_OPERATION), length=8) + BitArray(uint=OPERATION_SIZE_ENCODING(PIM_OPSIZE), length=5) + BitArray(uint=DATA_TYPE_ENCODING(PIM_DATA_TYPE), length=3) + BitArray(uint=int(RVU_SRC), length=5) + BitArray(uint=int(RVU_REG_SRC), length=14)+BitArray(uint=0, length=(128-(8+5+3+5+14)))
            ####################################
            output_file.write("    ###%s" % line)
            #output_file.write("    clflush\t%s \t\t\t#FLUSH TO KEEP CACHE COHERENCE\n" %DST)
            output_file.write("    pushq %rax\n")
            output_file.write("    mfence\n")
            output_file.write("    movq\t$0x%s" % base_instruction128bits.hex[0:16])
            output_file.write(", %rax\n")
            output_file.write("    movq\t%rax, %xmm0\n")
            output_file.write("    movq\t$0x%s" % base_instruction128bits.hex[16:32])
            output_file.write(", %rax\n")
            output_file.write("    movq\t%rax, %xmm1\n")
            output_file.write("    punpcklqdq\t%xmm1,%xmm0\n")
            output_file.write("    movntdq\t%xmm0,"+PIM_NAME+"(%rip)\n")
            output_file.write("    mfence\n")
            output_file.write("    #######################\n")
            if "%rax" in DST_to_SAVE and "%r10" not in DST_to_SAVE:
                output_file.write("    popq %rax\n")    
                output_file.write("    pushq %r10\n")
                output_file.write("    movq %rax, %r10\n")
            output_file.write("    leaq %s," %DST)
            output_file.write("%rax\t\t #CLFLUSH ADDRESS\n")
            output_file.write("    movq\t%rax,"+PIM_ADDR+"(%rip)\n")
            output_file.write("    #######################\n")
            if "%rax" in DST_to_SAVE and "%r10" not in DST_to_SAVE:
                output_file.write("    movq %r10, %rax\n")                
                output_file.write("    popq %r10\n")
                output_file.write("    pushq %rax\n")              
            output_file.write("    callq SimulatorCall\n")
            output_file.write("    #######################\n")
            output_file.write("    popq %rax\n")
            output_file.write("###### SIMULATION CALL ENDS ######\n")
            output_file.write("### END PIM STORE ###\n")
        ##############################################
        elif PIM_OPERATION=='VADD' or PIM_OPERATION=='VMUL' or PIM_OPERATION=='VXOR' or PIM_OPERATION=='VPERM':
            # print "GENERATING %s\n" %PIM_OPERATION
            SRC1 = PIM_INSTRUCTION[2].replace(',','')
            # print "SRC1 = %s" %SRC1
            RVU_SRC1 = SRC1.split('_')
            RVU_SRC1 = RVU_SRC1[1]
            # print "RVU_SRC1 = %s" %RVU_SRC1
            RVU_REG_SRC1 = SRC1.split('_')
            # print REG_SRC
            RVU_REG_SRC1 = RVU_REG_SRC1[-1]
            # print "RVU_REG_SRC1 = %s" %RVU_REG_SRC1

            SRC2 = PIM_INSTRUCTION[3].replace(',','')
            # print "SRC2 = %s" %SRC2
            RVU_SRC2 = SRC2.split('_')
            RVU_SRC2 = RVU_SRC2[1]
            # print "RVU_SRC2 = %s" %RVU_SRC2
            RVU_REG_SRC2 = SRC2.split('_')
            # print REG_SRC
            RVU_REG_SRC2 = RVU_REG_SRC2[-1]
            # print "RVU_REG_SRC2 = %s" %RVU_REG_SRC2

            DST = PIM_INSTRUCTION[4].replace(',','')
            # print "DST = %s" %DST
            RVU_DST = DST.split('_')
            RVU_DST = RVU_DST[1]
            # print "RVU_DST = %s" %RVU_DST
            RVU_REG_DST = DST.split('_')
            # print REG_DST
            RVU_REG_DST = RVU_REG_DST[-1]
            # print "RVU_REG_DST = %s" %RVU_REG_DST
            ####################################
            ###generate the instruction
            base_instruction128bits = BitArray(uint=INSTRUCTION_ENCODING(PIM_OPERATION), length=8) + BitArray(uint=OPERATION_SIZE_ENCODING(PIM_OPSIZE), length=5) + BitArray(uint=DATA_TYPE_ENCODING(PIM_DATA_TYPE), length=3) + BitArray(uint=int(RVU_DST), length=5) + BitArray(uint=int(RVU_REG_DST), length=14) + BitArray(uint=int(RVU_SRC1), length=5) + BitArray(uint=int(RVU_REG_SRC1), length=14) + BitArray(uint=int(RVU_SRC2), length=5) + BitArray(uint=int(RVU_REG_SRC2), length=14) + BitArray(uint=0, length=(128-(8+5+3+5+14+5+14+5+14)))
            ####################################
            output_file.write("    ###%s" % line)
            output_file.write("    pushq %rax\n")
            output_file.write("    mfence\n")
            output_file.write("    movq\t$0x%s" % base_instruction128bits.hex[0:16])
            output_file.write(", %rax\n")
            output_file.write("    movq\t%rax, %xmm0\n")
            output_file.write("    movq\t$0x%s" % base_instruction128bits.hex[16:32])
            output_file.write(", %rax\n")
            output_file.write("    movq\t%rax, %xmm1\n")
            output_file.write("    punpcklqdq\t%xmm1,%xmm0\n")
            output_file.write("    movntdq\t%xmm0,"+PIM_NAME+"(%rip)\n")
            output_file.write("    mfence\n")
            output_file.write("    #######################\n")
            output_file.write("    callq SimulatorCall\n")
            output_file.write("    #######################\n")
            output_file.write("    popq %rax\n")      
            output_file.write("### END PIM VADD ###\n")
        ##############################################

###############################################################################

    else:
        output_file.write("%s" % line)

###############################################################################
output_file.close()
###############################################################################

###############################################################################
BASE_ADDR = 0x404040

for i in range(1):
    fileHandle = open(output_asm, "r")
    lineList = fileHandle.readlines()
    fileHandle.close()
    lastLine = lineList[len(lineList)-2]
    # PIM_ADDR = BASE_ADDR
    lineList[len(lineList)-3] = '\t.globl	PIM_ADDR\n\t.data\n\t.align 16\n\t.type	PIM_ADDR,@object      # @PIM_ADDR\n\t.size	PIM_ADDR, 512\n\n\n'
    lineList[len(lineList)-2] = '\t.globl	PIM_LS\n\t.data\n\t.align 16\n\t.type	PIM_LS,@object      # @PIM_LS\n\t.size	PIM_LS, 512\n\n\n'
    # lineList[len(lineList)-3] = '\t.globl	PIM_ADDR\n\t.data\n\t.align 16\n\t.type	PIM_ADDR,@object      # @PIM_ADDR\n\t.size	PIM_ADDR, 512\nPIM_ADDR:\n\t.quad\t'+str(PIM_ADDR)+'\t#PIM_ADDRESS\n\n\n'
    # lineList[len(lineList)-2] = '\t.globl	PIM_LS\n\t.data\n\t.align 16\n\t.type	PIM_LS,@object      # @PIM_LS\n\t.size	PIM_LS, 512\nPIM_LS:\n\t.quad\t'+'\t#LS ADDRESSES\n\n\n'
    with open(output_asm, "w") as file:
        file.writelines(lineList)
###############################################################################
