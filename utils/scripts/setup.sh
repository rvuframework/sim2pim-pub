#!/bin/bash

KERNELDIR="../kernelRDPMC"

if modprobe msr ; then
  echo "MSR done";
else
  echo -e "\n\e[31mMSR failed, try with sudo\e[0m\n";
  exit $?
fi
if insmod $KERNELDIR/PMC_kernel.ko ; then
  echo "Kernel done";
else
  echo -e "\n\e[31mKernel Launch failed, run make on $KERNELDIR \e[0m\n";
  exit $?
fi
