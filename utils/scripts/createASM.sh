#!/bin/bash

###################################################
# Build Tree
###################################################

primo_build="/home/pc/Documents/PRIMO/primo_build"
#primo_build="/home/beforlin/pim/PRIMO/pim_v2/build"

BENCHMARK_FOLDER="../../benchmark"

IR="${BENCHMARK_FOLDER}/compiled/IR"
ASM="${BENCHMARK_FOLDER}/compiled/ASM"
ASM_FOLDER="${BENCHMARK_FOLDER}/compiled/ASM"

TESTS_SRC_FOLDER=$BENCHMARK_FOLDER
PARSED_TESTS_FOLDER="${BENCHMARK_FOLDER}/src"

PARSERDIR="../parser"

#create compile folder to store ASM IR, etc
mkdir -p ../../benchmark/compiled/ASM
mkdir -p ../../benchmark/compiled/IR
mkdir -p ../../benchmark/compiled/Obj
mkdir -p ../../benchmark/compiled/Dump
mkdir -p ../../benchmark/compiled/ExE

fullfile=$1
RVU_SIZE=$2

temp=0
cte=4

if [[ $# -lt 1  ]]; then
    echo "Illegal number of parameters"
    echo "SELECT THE BENCHMARK C FILE FOR SSE AND AVX COMPILE e.g., bash this_script VECSUM_XXX.s"
    echo "IF RVU, ALSO SELECT OPERATION SIZE e.g., bash this_script VECSUM_XXX.s 256"
    exit 2
fi

if [[ $# -eq 1 ]]; then
  SSE_COMPILING="SSE"
  AVX_COMPILING="AVX"
  GCC_COMPILIG="GCC"
fi

printf "===================================================\n"
printf "COMPILING %s " "$C_CODE_NAME"
printf "for the following archs\n"
if [[ $SSE_COMPILING == "SSE" ]];
then
	printf "SSE\n"
fi
if [[ $AVX_COMPILING == "AVX" ]];
then
	printf "AVX\n"
fi
if [[ $RVU_SIZE -ne 0 ]];
then
	printf "RVU %d Bytes\n" "$(($RVU_SIZE*$cte))"
fi
printf "\n"
printf "\n"

C_CODE_NAME=$(basename -- "$fullfile")
extension="${C_CODE_NAME##*.}"
C_CODE_NAME="${C_CODE_NAME%.*}"

# if ["$(dirname "${fullfile}")" > 0]; then
#   echo "Using custom Path"
# fi

if [[ $extension != "c" ]]; then
	printf "Not a C file!\n"
  exit 2
fi

##################### CHANGE MAIN TO FUNC TYPE(SIM INTEGRATION) ######################
if [[ $RVU_SIZE -ne 0 ]];
then
  funcType="vecsum"
  if python ./$PARSERDIR/createFunc.py $BENCHMARK_FOLDER/$fullfile $funcType ; then
    echo "Changed from main to $funcType"
    C_CODE_NAME="tempFile"
  else
    echo -e "\n\e[31mFunction Change failed\e[0m\n";
    exit $?
  fi
fi

clear

########################################################################
printf "...Compiling with LLVM for X86_SandyBridge, X86_KNL and X86_KNL+RVU...\n"

# printf "Memory Information\n"
# free -m
# printf "Disk Information\n"
# df -h
# printf "\n"
# printf "\n"

########################################################################



########################################################################

printf "Clang - Generating LLVM intermediate language file\n"
clang-4.0 -S -emit-llvm -march=knl -O3 -mllvm -disable-llvm-optzns $BENCHMARK_FOLDER/$C_CODE_NAME.c -o $IR/$C_CODE_NAME.ll
# clang-4.0 -S -emit-llvm -march=knl -O3 -mllvm -disable-llvm-optzns gem5_stats.c -o IR/gem5_stats.ll
printf "Clang - ll generated\n\n"

##################### OPTIMIZATION ######################
printf "LLVM Opt - Optimizing intermediate language file\n"
# /home/pcssjunior/llvm/llvm_build/bin/opt -S -O3 IR/gem5_stats.ll -o IR/gem5_stats_o3.ll
if [[ $RVU_SIZE -ne 0 ]]; then
    $primo_build/bin/opt -S -O3 -force-vector-width=$RVU_SIZE -pass-remarks=loop-vectorize -enable-load-pre=0 $IR/$C_CODE_NAME.ll -o $IR/$C_CODE_NAME\_o3\_$RVU_SIZE.ll
fi
if [[ $SSE_COMPILING == "SSE" ]] | [[ $AVX_COMPILING == "AVX" ]]; then
    $primo_build/bin/opt -S -O3 -pass-remarks=loop-vectorize -enable-load-pre=0 $IR/$C_CODE_NAME.ll -o $IR/$C_CODE_NAME\_o3\_X86.ll
fi
printf "LLVM Opt - ll o3 level optimization generated\n\n"

temp=$(($RVU_SIZE*$cte))

##################### ASM ######################
printf "LLC- Generating ASM codes\n"
# /$primo_build/bin/llc -filetype=asm -show-mc-encoding -x86-asm-syntax=intel -mcpu=sandybridge -mattr=-avx,-avx2,-avx512f IR/gem5_stats_o3.ll -o ./ASM/gem5_stats.s
if [[ $SSE_COMPILING == "SSE" ]]; then
	# $primo_build/bin/llc -filetype=asm -show-mc-encoding -x86-asm-syntax=intel -mcpu=sandybridge -mattr=-avx,-avx2,-avx512f IR/$C_CODE_NAME\_o3\_X86.ll -o ./ASM/$C_CODE_NAME\_SSE.s
	$primo_build/bin/llc -filetype=asm -show-mc-encoding -mcpu=sandybridge -mattr=-avx,-avx2,-avx512f $IR/$C_CODE_NAME\_o3\_X86.ll -o $ASM/$C_CODE_NAME\_SSE.s
fi
if [[ $AVX_COMPILING == "AVX" ]]; then
	# $primo_build/bin/llc -filetype=asm -show-mc-encoding -x86-asm-syntax=intel -mcpu=knl IR/$C_CODE_NAME\_o3\_X86.ll -o ./ASM/$C_CODE_NAME\_AVX.s
	$primo_build/bin/llc -filetype=asm -show-mc-encoding -mcpu=knl $IR/$C_CODE_NAME\_o3\_X86.ll -o $ASM/$C_CODE_NAME\_AVX.s
fi
if [[ $RVU_SIZE -ne 0 ]]; then
	# $primo_build/bin/llc -filetype=asm -show-mc-encoding -x86-asm-syntax=intel -mattr=+pim,+avx512bw IR/$C_CODE_NAME\_o3\_$RVU_SIZE.ll -o ./ASM/$C_CODE_NAME\_VPUOFF\_RVU$(($RVU_SIZE*$cte))B.s
	$primo_build/bin/llc -filetype=asm -show-mc-encoding -mattr=+pim,+avx512bw $IR/$C_CODE_NAME\_o3\_$RVU_SIZE.ll -o $ASM/$C_CODE_NAME\_$(($RVU_SIZE*$cte))B.s
fi
printf "LLC - ASM codes generated\n\n"

printf "ALL FINISHED\n\n\n"

#~ rm *.ll
#~ rm *.o

##################### MT PARSER ######################
NEW_TEMP=$C_CODE_NAME\_$(($RVU_SIZE*$cte))B.s

if python ./$PARSERDIR/mt_parser.py ./$ASM_FOLDER/$NEW_TEMP ./tempFile2.s; then
  echo "Parsed to ./tempFile2.s";
else
  echo -e "\n\e[31mParser failed\e[0m\n";
  exit $?
fi

##################### PARSER ######################
C_CODE_NAME=$(basename -- "$fullfile")
extension="${C_CODE_NAME##*.}"
C_CODE_NAME="${C_CODE_NAME%.*}"
NEW_BENCH=$C_CODE_NAME\_$(($RVU_SIZE*$cte))B.s

echo $NEW_BENCH
if python ./$PARSERDIR/instruction_parser.py ./tempFile2.s ./$PARSED_TESTS_FOLDER/x86_$NEW_BENCH ; then
  echo "Parsed to ./$PARSED_TESTS_FOLDER/x86_$NEW_BENCH";
else
  echo -e "\n\e[31mParser failed\e[0m\n";
  exit $?
fi

rm -f tempFile2.s
rm -f $BENCHMARK_FOLDER/tempFile.c
